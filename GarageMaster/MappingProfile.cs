﻿using System;
using System.Linq;
using AutoMapper;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.Models.AboutSiteSection.AboutProjectModels;
using GarageMaster.Models.AboutSiteSection.ContactsModels;
using GarageMaster.Models.AboutSiteSection.RulesModels;
using GarageMaster.Models.AccountModels;
using GarageMaster.Models.AdsSection.AdCategoryModels;
using GarageMaster.Models.AdsSection.AdsModels;
using GarageMaster.Models.Forum.CommentModels;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Models.RoleModels;

namespace GarageMaster
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            UniversalMap<User, AdminUserEditModel>();
            UniversalMap<User, UserDetailsModel>();
            UniversalMap<User, RegisterModel>();
            UniversalMap<User, UserEditModel>();
            UniversalMap<User, UserModel>();
            UniversalMap<Role, RoleModel>();
            UniversalMap<Role, RoleCreateModel>();
            UniversalMap<Role, RoleEditModel>();
            UniversalMap<Contact, ContactModel>();
            UniversalMap<Contact, ContactCreateModel>();
            UniversalMap<Contact, ContactEditModel>();
            UniversalMap<AboutProject, AboutProjectModel>();
            UniversalMap<AboutProject, AboutProjectCreateModel>();
            UniversalMap<AboutProject, AboutProjectEditModel>();
            UniversalMap<Rule, RuleModel>();
            UniversalMap<Rule, RuleCreateModel>();
            UniversalMap<Rule, RuleEditModel>();
            UniversalMap<AdCategory, AdCategoryModel>();
            UniversalMap<AdCategory, AdCategoryCreateModel>();
            UniversalMap<AdCategory, AdCategoryEditModel>();
            UniversalMap<AdCategory, AdCategoryDeleteModel>();
            //UniversalMap<Ad, AdModel>();
            //UniversalMap<Ad, AdCreateModel>();
            UniversalMap<Ad, AdEditModel>();
            UniversalMap<Ad, AdDeleteModel>();
            UniversalMap<Ad, AdEditAdminModel>();
            UniversalMap<Topic, TopicEditModel>();
            CreateRoleToRolesForUserModelMap();
            CreateUserToUsersForRoleModelMap();
            CreateRuleToRuleByAdminModelMap();
            CreateAboutProjectToAboutProjectByAdminModelMap();
            CreateContactToContactByAdminModelMap();
            CreateAdToAdModelMap();
            CreateAdCreateModelToAd();
            CreateAdToAdAdminModelMap();
            CreateTopicToTopicModelMap();
            CreateTopicToTopicDetailsModelMap();
            CreateTopicCreateModelToTopic();
            CreateCommentCreateModelToCommentMap();
            UniversalMap<Comment, CommentModel>();
            CreateCommentToCommentModelMap();
        }

        private void UniversalMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }

        private void CreateRoleToRolesForUserModelMap()
        {
            CreateMap<Role, RolesForUserModel>()
                .ForMember(target => target.RoleId,
                    src => src.MapFrom(r => r.Id))
                .ForMember(target => target.RoleName,
                    src => src.MapFrom(r => r.Name));
        }

        private void CreateUserToUsersForRoleModelMap()
        {
            CreateMap<User, UsersForRoleModel>()
                .ForMember(target => target.UserId,
                    src => src.MapFrom(u => u.Id))
                .ForMember(target => target.UserName,
                    src => src.MapFrom(u => u.UserName));
        }

        private void CreateRuleToRuleByAdminModelMap()
        {
            CreateMap<Rule, RuleByAdminModel>()
                .ForMember(target => target.Id,
                    src => src.MapFrom(ru => ru.Id))
                .ForMember(target => target.Title,
                    src => src.MapFrom(ru => ru.Title))
                .ForMember(target => target.Content,
                    src => src.MapFrom(ru => ru.Content))
                .ForMember(target => target.IsCreated,
                    src => src.MapFrom(ru => true));
        }

        private void CreateAboutProjectToAboutProjectByAdminModelMap()
        {
            CreateMap<AboutProject, AboutProjectByAdminModel>()
                .ForMember(target => target.Id,
                    src => src.MapFrom(ap => ap.Id))
                .ForMember(target => target.Title,
                    src => src.MapFrom(ap => ap.Title))
                .ForMember(target => target.Content,
                    src => src.MapFrom(ap => ap.Content))
                .ForMember(target => target.IsCreated,
                    src => src.MapFrom(ap => true));
        }

        private void CreateContactToContactByAdminModelMap()
        {
            CreateMap<Contact, ContactByAdminModel>()
                .ForMember(target => target.Id,
                    src => src.MapFrom(c => c.Id))
                .ForMember(target => target.Title,
                    src => src.MapFrom(c => c.Title))
                .ForMember(target => target.Content,
                    src => src.MapFrom(c => c.Content))
                .ForMember(target => target.Address,
                    src => src.MapFrom(c => c.Address))
                .ForMember(target => target.Email,
                    src => src.MapFrom(c => c.Email))
                .ForMember(target => target.PhoneNumber,
                    src => src.MapFrom(c => c.PhoneNumber))
                .ForMember(target => target.FilePath,
                    src => src.MapFrom(c => c.FilePath))
                .ForMember(target => target.IsCreated,
                    src => src.MapFrom(c => true));
        }

        private void CreateAdToAdModelMap()
        {
            CreateMap<Ad, AdModel>()
                .ForMember(target => target.Title,
                    src => src.MapFrom(a => a.Title))
                .ForMember(target => target.Content,
                    src => src.MapFrom(a => a.Content))
                .ForMember(target => target.AdPictures,
                    src => src.MapFrom(a => a.Pictures))
                .ForMember(target => target.DateOfPublication,
                src => src.MapFrom(a => a.DateOfPublication.ToString("F")))
                .ForMember(target => target.PickUpDate,
                src => src.MapFrom(a => a.PickupDate.ToString("F")))
                .ForMember(target => target.User,
                src => src.MapFrom(a => a.User))
                .ForMember(target => target.QtyPhotos,
                src => src.MapFrom(a => a.Pictures.Count(ap => ap.AdId == a.Id)));
        }

        public void CreateAdCreateModelToAd()
        {
            CreateMap<AdCreateModel, Ad>()
                .ForMember(target => target.DateOfPublication,
                    src => src.MapFrom(t => DateTime.Now.ToLongTimeString()));
        }

        private void CreateAdToAdAdminModelMap()
        {
            CreateMap<Ad, AdAdminModel>()
                .ForMember(target => target.Title,
                    src => src.MapFrom(a => a.Title))
                .ForMember(target => target.Content,
                    src => src.MapFrom(a => a.Content))
                .ForMember(target => target.DateOfPublication,
                    src => src.MapFrom(a => a.DateOfPublication.ToString("F")))
                .ForMember(target => target.PickUpDate,
                    src => src.MapFrom(a => a.PickupDate.ToString("F")))
                .ForMember(target => target.User,
                    src => src.MapFrom(a => a.User))
                .ForMember(target => target.QtyPhotos,
                    src => src.MapFrom(a => a.Pictures.Count(ap => ap.AdId == a.Id)));
        }

        private void CreateTopicToTopicModelMap()
        {
            CreateMap<Topic, TopicModel>()
                .ForMember(target => target.TopicTitle,
                    src => src.MapFrom(t => t.TopicTitle))
                .ForMember(target => target.TopicContent,
                    src => src.MapFrom(t => t.TopicContent))
                .ForMember(target => target.TopicPictures,
                    src => src.MapFrom(t => t.Pictures))
                .ForMember(target => target.TopicCreationDate,
                    src => src.MapFrom(t => t.TopicCreationDate.ToString("F")))
                .ForMember(target => target.Author,
                    src => src.MapFrom(t => t.Author.NickName))
                .ForMember(target => target.CommentsQty,
                    src => src.MapFrom(t => t.Comments.Count(c => c.TopicId == t.Id)));
        }

        private void CreateTopicToTopicDetailsModelMap()
        {
            CreateMap<Topic, TopicDetailsModel>()
                .ForMember(target => target.TopicTitle,
                    src => src.MapFrom(t => t.TopicTitle))
                .ForMember(target => target.TopicContent,
                    src => src.MapFrom(t => t.TopicContent))
                .ForMember(target => target.TopicPictures,
                    src => src.MapFrom(t => t.Pictures))
                .ForMember(target => target.TopicCreationDate,
                    src => src.MapFrom(t => t.TopicCreationDate.ToString("F")))
                .ForMember(target => target.Author,
                    src => src.MapFrom(t => t.Author.NickName))
                .ForMember(target => target.CommentsQty,
                    src => src.MapFrom(t => t.Comments.Count(c => c.TopicId == t.Id)))
                .ForMember(target => target.Comments,
                    src => src.MapFrom(t => t.Comments.Where(c => c.Type == CommentType.ToTopic).ToList()));
        }

        public void CreateTopicCreateModelToTopic()
        {
            CreateMap<TopicCreateModel, Topic>()
                .ForMember(target => target.TopicCreationDate,
                    src => src.MapFrom(t => DateTime.Now.ToLongTimeString()));
        }

        private void CreateCommentCreateModelToCommentMap()
        {
            CreateMap<CommentCreateModel, Comment>()
                .ForMember(target => target.CommentContent,
                    src => src.MapFrom(c => c.CommentContent))
                .ForMember(target => target.CommentCreationDate,
                    src => src.MapFrom(c => DateTime.Now.ToLongTimeString()));
        }

        private void CreateCommentToCommentModelMap()
        {
            CreateMap<Comment, CommentModel>()
                .ForMember(target => target.CommentCreationDate,
                    src => src.MapFrom(p => p.CommentCreationDate.ToString()))
                .ForMember(target => target.Author,
                    src => src.MapFrom(p => p.Author.NickName))
                .ForMember(target => target.Comments,
                    src => src.MapFrom(p => p.SubComments))
                .ForMember(target => target.Topic,
                    src => src.MapFrom(p => p.Topic.TopicTitle))
                .ForMember(target => target.UpperComment,
                    src => src.MapFrom(p => p.UpperComment.CommentContent));
        }
    }
}
