using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.EntitiesConfiguration;
using GarageMaster.DAL.EntitiesConfiguration.Contracts;
using GarageMaster.Services.AboutSiteSection.AboutProjects;
using GarageMaster.Services.AboutSiteSection.AboutProjects.Contracts;
using GarageMaster.Services.AboutSiteSection.Contacts;
using GarageMaster.Services.AboutSiteSection.Contacts.Contracts;
using GarageMaster.Services.AboutSiteSection.Rules;
using GarageMaster.Services.AboutSiteSection.Rules.Contracts;
using GarageMaster.Services.Account;
using GarageMaster.Services.Account.Contracts;
using GarageMaster.Services.AdsSection.AdCategories;
using GarageMaster.Services.AdsSection.AdCategories.Contracts;
using GarageMaster.Services.AdsSection.Ads;
using GarageMaster.Services.AdsSection.Ads.Contracts;
using GarageMaster.Services.Forum.Comments;
using GarageMaster.Services.Forum.Comments.Contracts;
using GarageMaster.Services.Forum.Topics;
using GarageMaster.Services.Forum.Topics.Contracts;
using GarageMaster.Services.Roles;
using GarageMaster.Services.Roles.Contracts;
using GarageMaster.Validation.Validation_Entities.Account;
using GarageMaster.Validation.Validation_Entities.AdsSection.AdCatagories;
using GarageMaster.Validation.Validation_Entities.AdsSection.Ads;
using GarageMaster.Validation.Validation_Entities.Forum;
using GarageMaster.Validation.Validation_Entities.Roles;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GarageMaster
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("MainConnectionString");

            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseNpgsql(connectionString);

            //adds ApplicationContext and User Identity
            services.AddScoped<IEntityConfigurationsContainer>(sp => new EntityConfigurationsContainer());

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });

            // add AppDbContext && UoW && Identity
            services.AddSingleton<IApplicationDbContextFactory>(sp => new ApplicationDbContextFactory(optionsBuilder.Options, new EntityConfigurationsContainer()));
            services.AddSingleton<IUnitOfWorkFactory, UnitOfWorkFactory>();
            services.AddIdentity<User, Role>(options => options.SignIn.RequireConfirmedAccount = false)
                .AddDefaultTokenProviders()
                .AddDefaultUI()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            //adds services
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<RegisterValidator>();
            services.AddTransient<UserEditValidator>();
            services.AddTransient<AdminUserEditValidator>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<RoleCreateValidator>();
            services.AddTransient<RoleEditValidator>();
            services.AddTransient<IContactsService, ContactsService>();
            services.AddTransient<IAboutProjectsService, AboutProjectsService>();
            services.AddTransient<IRulesService, RulesService>();
            services.AddTransient<IAdCategoryService, AdCategoryService>();
            services.AddTransient<AdCategoryCreateValidator>();
            services.AddTransient<AdCategoryEditValidator>();
            services.AddTransient<IAdsService, AdsService>();
            services.AddTransient<AdCreateValidator>();
            services.AddTransient<AdEditValidator>();
            services.AddTransient<AdEditAdminValidator>();
            services.AddTransient<ITopicService, TopicService>();
            services.AddTransient<TopicCreateValidator>();
            services.AddTransient<TopicEditValidator>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<CommentCreateValidator>();
            services.AddTransient<CommentEditValidator>();
            //services.AddSingleton<IEventService, EventService>();
            //services.AddTransient<IDepartmentService, DepartmentService>();
            //services.AddTransient<IPositionService, PositionService>();
            //services.AddSingleton<INewsService, NewsService>();

            ////adds Policy for users and roles
            //services.AddAuthorization(opts => {
            //    opts.AddPolicy("User", policy =>
            //    {
            //        policy.RequireAuthenticatedUser();
            //    });
            //}); services.AddAuthorization(opts => {
            //    opts.AddPolicy("Admin", policy =>
            //    {
            //        policy.RequireAuthenticatedUser();
            //        policy.RequireRole("Admin");
            //    });
            //});

            //adds controllers
            services.AddControllersWithViews();
            services.AddRazorPages();

            Mapper.Initialize(config => config.AddProfile(new MappingProfile()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    "admin",
                    "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            //ApplicationDbInitializer.SeedRoles(roleManager).ConfigureAwait(false).GetAwaiter().GetResult();
            //ApplicationDbInitializer.SeedUsers(userManager).ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}
