﻿using Microsoft.AspNetCore.Mvc;
using System;
using GarageMaster.Models.AdsSection.AdsModels;
using GarageMaster.Services.AdsSection.Ads.Contracts;
using GarageMaster.Validation;
using Microsoft.AspNetCore.Authorization;

namespace GarageMaster.Areas.Admin.Controllers.AdsSection
{
    [Area("Admin")]
    public class AdsController : Controller
    {
        private readonly IAdsService _adsService;

        public AdsController(IAdsService adsService)
        {
            if (adsService == null)
                throw new ArgumentNullException(nameof(adsService));

            _adsService = adsService;
        }
        
        [HttpGet]
        public IActionResult IndexAdmin(AdFilterAdminModel model)
        {
            try
            {
                var ads = _adsService.GetAdFilterAdminModel(model);
                return View(ads);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        public IActionResult EditAdAdmin(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Ad id cannot be NULL";
                return View("BadRequest");
            }
            var adEditAdminModel = _adsService.GetAdEditAdminModel(id.Value);

            return View(adEditAdminModel);
        }

        [HttpPost]
        [Authorize]
        public IActionResult EditAdAdmin(AdEditAdminModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _adsService.EditAdAdmin(model);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("IndexAdmin", "Ads");
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(EditAdAdmin), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        public IActionResult DeleteAdAdmin(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Ad id cannot be NULL";
                return View("BadRequest");
            }
            var adDeleteModel = _adsService.GetAdDeleteModel(id.Value);
            return View(adDeleteModel);
        }

        [HttpPost]
        [Authorize]
        public IActionResult DeleteAdAdmin(AdDeleteModel model)
        {
            try
            {
                _adsService.DeleteAd(model);

                return RedirectToAction("IndexAdmin", "Ads");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
