﻿using Microsoft.AspNetCore.Mvc;
using System;
using GarageMaster.Models.AdsSection.AdCategoryModels;
using GarageMaster.Services.AdsSection.AdCategories.Contracts;
using GarageMaster.Validation;

namespace GarageMaster.Areas.Admin.Controllers.AdsSection
{
    [Area("Admin")]
    public class AdCategoryController : Controller
    {
        private IAdCategoryService _adCategoryService;

        public AdCategoryController(IAdCategoryService adCategoryService)
        {
            if (adCategoryService == null)
                throw new ArgumentNullException(nameof(adCategoryService));

            _adCategoryService = adCategoryService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = _adCategoryService.GetAdCategoriesList();
            return View(model);
        }

        [HttpGet]
        public IActionResult CreateAdCategory()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateAdCategory(AdCategoryCreateModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _adCategoryService.CreateAdCategory(model);
                if (result.IsSuccess)
                {
                    return RedirectToAction("Index", "AdCategory");
                }
                else
                {
                    UpdateModelState(result);

                    return View(nameof(CreateAdCategory), model);
                }
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult EditAdCategory(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Ad Category id cannot be NULL";
                return View("BadRequest");
            }

            var adCategoryEditModel = _adCategoryService.GetAdCategoryById(id.Value);

            return View(adCategoryEditModel);
        }

        [HttpPost]
        public IActionResult EditAdCategory(AdCategoryEditModel model)
        {
            try
            {
                var result = _adCategoryService.EditAdCategory(model);
                if (result.IsSuccess)
                {
                    return RedirectToAction("Index", "AdCategory");
                }
                else
                {
                    UpdateModelState(result);

                    return View(nameof(EditAdCategory), model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public IActionResult DeleteAdCategory(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Ad Category id cannot be NULL";
                return View("BadRequest");
            }

            var adCategoryDeleteModel = _adCategoryService.GetAdCategoryByIdForDelete(id.Value);
            ViewBag.AttentionMessage = "Attention! Removing this element can disrupt the interconnection of data and affect the performance of the site!";

            return View(adCategoryDeleteModel);
        }

        [HttpPost]
        public IActionResult DeleteAdCategory(AdCategoryDeleteModel model)
        {
            try
            {
                _adCategoryService.DeleteAdCategory(model);

                return RedirectToAction("Index", "AdCategory");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
