﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GarageMaster.Models.RoleModels;
using GarageMaster.Services.Roles.Contracts;
using GarageMaster.Validation;

namespace GarageMaster.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet]
        public IActionResult Roles(RolesModel model)
        {
            var roles = _roleService.GetListRolesForAdmin(model);

            return View(roles);
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            try
            {
                var roleCreateModel = _roleService.GetRoleCreateModel();
                return View(roleCreateModel);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(RoleCreateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _roleService.CreateRole(model);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("Roles", "Role");
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(CreateRole), model);
                    }
                }
                return View(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> ProfileRole(Guid id)
        {
            var model = await _roleService.GetRoleForEditAsync(id);

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditRoleByAdmin(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Role id cannot be NULL";
                return View("BadRequest");
            }

            var roleEditModel = await _roleService.GetRoleEditModelAsync(id.Value);

            return View(roleEditModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditRoleByAdmin(RoleEditModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _roleService.EditRoleAdminAsync(model);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("ProfileRole", "Role", new { id = model.Id });
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(EditRoleByAdmin), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> AddUsersToRole(Guid id)
        {
            ViewBag.roleId = id;

            var usersForRoleModels = await _roleService.GetUsersForRoleAsync(id);

            return View(usersForRoleModels);
        }

        [HttpPost]
        public async Task<IActionResult> AddUsersToRole(List<UsersForRoleModel> model, Guid roleId)
        {
            await _roleService.UpdateUsersForRoleAsync(model, roleId);

            return RedirectToAction("ProfileRole", "Role", new { Id = roleId });
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
