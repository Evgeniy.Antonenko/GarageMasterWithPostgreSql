﻿using System;
using GarageMaster.Models.AboutSiteSection.AboutProjectModels;
using GarageMaster.Services.AboutSiteSection.AboutProjects.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace GarageMaster.Areas.Admin.Controllers.AboutSiteSection
{
    [Area("Admin")]
    public class AboutProjectController : Controller
    {
        private readonly IAboutProjectsService _aboutProjectsService;

        public AboutProjectController(IAboutProjectsService aboutProjectsService)
        {
            if (aboutProjectsService == null)
                throw new ArgumentNullException(nameof(aboutProjectsService));

            _aboutProjectsService = aboutProjectsService;
        }

        public IActionResult Index(AboutProjectByAdminModel model)
        {
            var aboutProjectByAdminModel = _aboutProjectsService.GetAboutProjectByAdminModel(model);

            return View(aboutProjectByAdminModel);
        }

        [HttpGet]
        public IActionResult CreateAboutProject()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateAboutProject(AboutProjectCreateModel model)
        {
            if (ModelState.IsValid)
            {
                _aboutProjectsService.CreateAboutProject(model);
                return RedirectToAction("Index", "AboutProject");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult EditAboutProject()
        {
            var model = _aboutProjectsService.GetAboutProjectForEdit();

            return View(model);
        }

        [HttpPost]
        public IActionResult EditAboutProject(AboutProjectEditModel model)
        {
            if (ModelState.IsValid)
            {
                _aboutProjectsService.EditAboutProject(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }
    }
}
