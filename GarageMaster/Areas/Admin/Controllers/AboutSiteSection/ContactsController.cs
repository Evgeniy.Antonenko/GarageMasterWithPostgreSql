﻿using Microsoft.AspNetCore.Mvc;
using GarageMaster.Models.AboutSiteSection.ContactsModels;
using GarageMaster.Services.AboutSiteSection.Contacts.Contracts;

namespace GarageMaster.Areas.Admin.Controllers.AboutSiteSection
{
    [Area("Admin")]
    public class ContactsController : Controller
    {
        private readonly IContactsService _contactsService;

        public ContactsController(IContactsService contactsService)
        {
            _contactsService = contactsService;
        }

        [HttpGet]
        public IActionResult Index(ContactByAdminModel model)
        {
            var contact = _contactsService.GetContactByAdminModel(model);
            return View(contact);
        }

        [HttpGet]
        public IActionResult CreateContact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateContact(ContactCreateModel model)
        {
            if (ModelState.IsValid)
            {
                _contactsService.CreateContact(model);
                return RedirectToAction("Index", "Contacts");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult EditContact()
        {
            var model = _contactsService.GetContactForEdit();

            return View(model);
        }

        [HttpPost]
        public IActionResult EditContact(ContactEditModel model)
        {
            if (ModelState.IsValid)
            {
                _contactsService.EditContact(model);
                return RedirectToAction("Index", "Contacts");
            }
            else
            {
                return View(model);
            }

        }
    }
}
