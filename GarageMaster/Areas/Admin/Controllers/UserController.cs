﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AccountModels;
using GarageMaster.Services.Account.Contracts;
using GarageMaster.Validation;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IAccountService _accountService;

        public UserController(UserManager<User> userManager, IAccountService accountService)
        {
            _userManager = userManager;
            _accountService = accountService;
        }

        [HttpGet]
        public IActionResult Users(UserFilterModel model)
        {

            var users = _accountService.GetListUsersForAdmin(model);

            return View(users);
        }

        [HttpGet]
        public async Task<IActionResult> ProfileUser(Guid id)
        {
            var user = await _accountService.GetUserModelForAdmin(id);
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> EditUserByAdmin(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "User id cannot be NULL";
                return View("BadRequest");
            }

            var adminUserEditModel = await _accountService.GetUserForAdminEditModel(id.Value);

            return View(adminUserEditModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditUserByAdmin(AdminUserEditModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _accountService.AdminEditUser(model);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("ProfileUser", "User", new { id = model.Id });
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(EditUserByAdmin), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public IActionResult ChangePasswordAdmin(Guid id)
        {
            var changePasswordAdminModel = new ChangePasswordAdminModel();
            changePasswordAdminModel.UserId = Convert.ToString(id);
            return View(changePasswordAdminModel);
        }

        ////нужно переделать метод
        [HttpPost]
        public async Task<IActionResult> ChangePasswordAdmin(ChangePasswordAdminModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.UserId);
                if (user != null)
                {
                    var _passwordValidator = HttpContext.RequestServices.GetService(typeof(IPasswordValidator<User>)) as IPasswordValidator<User>;
                    var _passwordHasher = HttpContext.RequestServices.GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;

                    IdentityResult result = await _passwordValidator.ValidateAsync(_userManager, user, model.NewPassword);
                    if (result.Succeeded)
                    {
                        user.PasswordHash = _passwordHasher.HashPassword(user, model.NewPassword);
                        await _userManager.UpdateAsync(user);
                        return RedirectToAction("ProfileUser", "User", new { id = model.UserId });
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Пользователь не найден");
                }
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> AddRolesToUser(Guid id)
        {
            ViewBag.userId = id;

            var rolesForUserModels = await _accountService.GetRolesForUserAsync(id);

            return View(rolesForUserModels);
        }

        [HttpPost]
        public async Task<IActionResult> AddRolesToUser(List<RolesForUserModel> model, Guid userId)
        {
            await _accountService.UpdateRolesForUserAsync(model, userId);

            return RedirectToAction("ProfileUser", "User", new { Id = userId });
        }


        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
