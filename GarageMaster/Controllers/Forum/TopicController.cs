﻿using System;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Services.Forum.Topics.Contracts;
using GarageMaster.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace GarageMaster.Controllers.Forum
{
    public class TopicController : Controller
    {
        private readonly ITopicService _topicService;
        private readonly UserManager<User> _userManager;

        public TopicController(ITopicService topicService, UserManager<User> userManager)
        {
            if (topicService == null)
                throw new ArgumentNullException(nameof(topicService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _topicService = topicService;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index(TopicFilterModel model)
        {
            try
            {
                model.CurrentUser = await _userManager.GetUserAsync(User);
                var topics = _topicService.GetTopicFilterModel(model);
                return View(topics);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult CreateTopic()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateTopic(TopicCreateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    User currentUser = await _userManager.GetUserAsync(User);
                    var result = _topicService.CreateTopic(model, currentUser.Id);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("Index", "Topic");
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(CreateTopic), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> DetailsTopic(Guid id)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var topicModel = _topicService.GetTopicById(id, currentUser);

            return View(topicModel);
        }

        [Authorize]
        [HttpGet]
        public IActionResult EditTopic(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Topic id cannot be NULL";
                return View("BadRequest");
            }

            var topicEditModel = _topicService.GetTopicEditModelById(id.Value);

            return View(topicEditModel);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> EditTopic(TopicEditModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _topicService.EditTopic(model, (await _userManager.GetUserAsync(User)).Id);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("DetailsTopic", "Topic", new { id = model.Id });
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(EditTopic), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
