﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.Models.Forum.CommentModels;
using GarageMaster.Services.Forum.Comments.Contracts;
using GarageMaster.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.Controllers.Forum
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly UserManager<User> _userManager;

        public CommentController(ICommentService commentService, UserManager<User> userManager)
        {
            if (commentService == null)
                throw new ArgumentNullException(nameof(commentService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _commentService = commentService;
            _userManager = userManager;
        }

        [Authorize]
        [HttpGet]
        public IActionResult CreateCommentToTopic(Guid? topicId)
        {
            if (!topicId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(topicId));

            var model = _commentService.GetCommentToTopicCreateModel(topicId.Value);

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult CreateCommentToComment(Guid? Id)
        {
            if (!Id.HasValue)
                throw new ArgumentOutOfRangeException(nameof(Id));

            var model = _commentService.GetCommentToCommentCreateModel(Id.Value);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateComment(CommentCreateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    User currentUser = await _userManager.GetUserAsync(User);
                    var result = _commentService.CreateComment(model, currentUser.Id);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("Index", "Topic");
                    }
                    else
                    {
                        UpdateModelState(result);
                        return model.Type == CommentType.ToTopic ? View(nameof(CreateCommentToTopic), model) : View(nameof(CreateCommentToTopic), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
