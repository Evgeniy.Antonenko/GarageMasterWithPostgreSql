﻿using Microsoft.AspNetCore.Mvc;
using System;
using GarageMaster.Services.AboutSiteSection.Contacts.Contracts;

namespace GarageMaster.Controllers.AboutSiteSection
{
    public class ContactsController : Controller
    {
        private readonly IContactsService _contactsService;

        public ContactsController(IContactsService contactsService)
        {
            if (contactsService == null)
                throw new ArgumentNullException(nameof(contactsService));

            _contactsService = contactsService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var contactModel = _contactsService.GetContact();

            return View(contactModel);
        }
    }
}
