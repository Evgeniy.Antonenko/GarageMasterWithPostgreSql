﻿using Microsoft.AspNetCore.Mvc;
using System;
using GarageMaster.Services.AboutSiteSection.AboutProjects.Contracts;

namespace GarageMaster.Controllers.AboutSiteSection
{
    public class AboutProjectController : Controller
    {
        private readonly IAboutProjectsService _aboutProjectsService;

        public AboutProjectController(IAboutProjectsService aboutProjectsService)
        {
            if (aboutProjectsService == null)
                throw new ArgumentNullException(nameof(aboutProjectsService));

            _aboutProjectsService = aboutProjectsService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var aboutProjectModel = _aboutProjectsService.GetAboutProject();

            return View(aboutProjectModel);
        }
    }
}
