﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AdsSection.AdsModels;
using GarageMaster.Services.AdsSection.Ads.Contracts;
using GarageMaster.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.Controllers.AdsSection
{
    public class AdsController : Controller
    {
        private readonly IAdsService _adsService;
        private readonly UserManager<User> _userManager;

        public AdsController(IAdsService adsService, UserManager<User> userManager)
        {
            if (adsService == null)
                throw new ArgumentNullException(nameof(adsService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _adsService = adsService;
            _userManager = userManager;
        }
        
        [HttpGet]
        public async Task<IActionResult> Index(AdFilterModel model)
        {
            try
            {
                model.CurrentUser = await _userManager.GetUserAsync(User);
                var ads = _adsService.GetAdFilterModel(model);
                return View(ads);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        public IActionResult CreateAd()
        {
            var adCreateModel = _adsService.GetAdCreateModel();
            return View(adCreateModel);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateAd(AdCreateModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _adsService.CreateAd(model, await _userManager.GetUserAsync(User));
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("Index", "Ads");
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(CreateAd), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        public IActionResult EditAd(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Ad id cannot be NULL";
                return View("BadRequest");
            }
            var adEditModel = _adsService.GetAdEditModel(id.Value);

            return View(adEditModel);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> EditAd (AdEditModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = _adsService.EditAd(model, (await _userManager.GetUserAsync(User)).Id);
                    if (result.IsSuccess)
                    {
                        return RedirectToAction("DetailsAd", "Ads", new { id = model.Id });
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(EditAd), model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> DetailsAd(Guid id)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            //Guid? currentUserId = currentUser.Id;
            var adModel = _adsService.GetAdById(id, currentUser);

            return View(adModel);
        }

        [Authorize]
        public IActionResult PickUpDate(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Ad id cannot be NULL";
                return View("BadRequest");
            }
            _adsService.MakePickUpDate(id.Value);

            return RedirectToAction("DetailsAd", "Ads", new { id = id });
        }

        [HttpGet]
        [Authorize]
        public IActionResult DeleteAd(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Ad id cannot be NULL";
                return View("BadRequest");
            }
            var adDeleteModel = _adsService.GetAdDeleteModel(id.Value);
            return View(adDeleteModel);
        }

        [HttpPost]
        [Authorize]
        public IActionResult DeleteAd(AdDeleteModel model)
        {
            try
            {
                _adsService.DeleteAd(model);

                return RedirectToAction("Index", "Ads");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
