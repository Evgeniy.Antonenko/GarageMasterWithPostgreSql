﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AccountModels;
using GarageMaster.Services.Account.Contracts;
using GarageMaster.Validation;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IAccountService _accountService;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IAccountService accountService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _accountService = accountService;
        }

        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> My()
        {
            try
            {
                User user = await _userManager.GetUserAsync(User);
                UserModel userModel = _accountService.GetUserModel(user);
                return View(userModel);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        //[Authorize]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel model)
        {
            User user = await _userManager.GetUserAsync(User);
            var result = await _userManager.ChangePasswordAsync(user, model.Password, model.NewPassword);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return View();
            }
            await _signInManager.RefreshSignInAsync(user);
            return RedirectToAction("My", "Account");
        }

        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> Edit()
        {
            try
            {
                User user = await _userManager.GetUserAsync(User);
                UserEditModel userEditModel = _accountService.GetUserEditModel(user);
                return View(userEditModel);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> Edit(UserEditModel model)
        {
            try
            {
                var result = await _accountService.EditUser(model, await _userManager.GetUserAsync(User));
                if (result.IsSuccess)
                {
                    return RedirectToAction("My", "Account");
                }
                else
                {
                    UpdateModelState(result);

                    return View(nameof(Edit), model);
                }

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public IActionResult Register()
        {
            try
            {
                RegisterModel registerModel = _accountService.GetRegisterModel();
                return View(registerModel);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _accountService.CreateUser(model);
                    if (result.IsSuccess)
                    {
                        var user = _userManager.Users.FirstOrDefault(u => u.Email == model.Email);
                        await _signInManager.SignInAsync(user, false);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        UpdateModelState(result);

                        return View(nameof(Register), model);
                    }
                }
                return View(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            try
            {
                return View(new LoginModel { ReturnUrl = returnUrl });
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result =
                        await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                    if (result.Succeeded)
                    {
                        // проверяем, принадлежит ли URL приложению
                        if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                        {
                            return Redirect(model.ReturnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                    }
                }
                return View(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            try
            {
                // удаляем аутентификационные куки
                await _signInManager.SignOutAsync();
                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private void UpdateModelState(ValidationResult result)
        {
            foreach (var message in result.ErrorMessages)
            {
                ModelState.AddModelError(string.Empty, message);
            }
        }
    }
}
