﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageMaster.Models.AboutSiteSection.AboutProjectModels
{
    public class AboutProjectByAdminModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Заголовок:")]
        public string Title { get; set; }

        [Display(Name = "Контент:")]
        public string Content { get; set; }

        public bool IsCreated { get; set; }
    }
}
