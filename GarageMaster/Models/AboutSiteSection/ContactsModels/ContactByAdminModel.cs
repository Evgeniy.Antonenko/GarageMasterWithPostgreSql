﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageMaster.Models.AboutSiteSection.ContactsModels
{
    public class ContactByAdminModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Заголовок:")]
        public string Title { get; set; }

        [Display(Name = "Контент:")]
        public string Content { get; set; }

        [Display(Name = "Адрес:")]
        public string Address { get; set; }

        [Display(Name = "e-mail:")]
        public string Email { get; set; }

        [Display(Name = "Номер телефона:")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Схема расположения")]
        public string FilePath { get; set; }

        public bool IsCreated { get; set; }
    }
}
