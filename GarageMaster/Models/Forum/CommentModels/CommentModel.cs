﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities.Forum;

namespace GarageMaster.Models.Forum.CommentModels
{
    public class CommentModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Дата публикации Комментария")]
        public DateTime CommentCreationDate { get; set; }

        [Display(Name = "Комментарий")]
        public string CommentContent { get; set; }

        [Display(Name = "Автор комментария")]
        public string Author { get; set; }

        [Display(Name = "Тема комментария")]
        public string Topic { get; set; }

        [Display(Name = "ID родительского коментария")]
        public Guid? UpperCommentId { get; set; }

        [Display(Name = "ID родительской темы")]
        public Guid TopicId { get; set; }

        [Display(Name = "Комментарий комментария")]
        public string UpperComment { get; set; }

        [Display(Name = "Уровень коментария")]
        public int CommentLevel { get; set; }

        [Display(Name = "Тип коментария")]
        public CommentType Type { get; set; }

        public List<CommentModel> Comments { get; set; }
    }
}
