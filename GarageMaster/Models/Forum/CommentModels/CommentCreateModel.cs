﻿using System;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Validation.Contracts;
using Microsoft.AspNetCore.Http;

namespace GarageMaster.Models.Forum.CommentModels
{
    public class CommentCreateModel : IValidated
    {
        [Required(ErrorMessage = "Укажите содержание комментария")]
        [Display(Name = "Комментарий")]
        public string CommentContent { get; set; }

        public Guid TopicId { get; set; }

        [Display(Name = "Тема комментария")]
        public string Topic { get; set; }

        public Guid? UpperCommentId { get; set; }

        [Display(Name = "Уровень коментария")]
        public int CommentLevel { get; set; }

        [Display(Name = "Тип коментария")]
        public CommentType Type { get; set; }

        public TopicModel TopicModel { get; set; }

        [Display(Name = "Добавить фотографии")]
        public IFormFileCollection Images { get; set; }
    }
}
