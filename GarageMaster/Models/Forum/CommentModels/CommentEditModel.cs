﻿using System;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.Validation.Contracts;

namespace GarageMaster.Models.Forum.CommentModels
{
    public class CommentEditModel : IValidated
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Дата публикации Комментария")]
        public string CommentCreationDate { get; set; }

        [Required(ErrorMessage = "Укажите содержание комментария")]
        [Display(Name = "Комментарий")]
        public string CommentContent { get; set; }

        [Display(Name = "Автор комментария")]
        public string Author { get; set; }

        public Guid TopicId { get; set; }

        public Guid? UpperCommentId { get; set; }

        [Display(Name = "Уровень коментария")]
        public int CommentLevel { get; set; }

        [Display(Name = "Тип коментария")]
        public CommentType Type { get; set; }
    }
}
