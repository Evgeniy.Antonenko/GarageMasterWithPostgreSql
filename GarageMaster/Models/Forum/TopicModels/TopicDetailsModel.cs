﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Entities.Pictures;
using GarageMaster.Models.Forum.CommentModels;

namespace GarageMaster.Models.Forum.TopicModels
{
    public class TopicDetailsModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Дата создания темы")]
        public DateTime TopicCreationDate { get; set; }

        [Display(Name = "Заголовок темы")]
        public string TopicTitle { get; set; }

        [Display(Name = "Содержание темы")]
        public string TopicContent { get; set; }

        public Guid AuthorId { get; set; }

        [Display(Name = "Автор темы")]
        public string Author { get; set; }

        [Display(Name = "Кол-во комментавиев")]
        public int CommentsQty { get; set; }

        [Display(Name = "Состояние темы")]
        public bool IsLocked { get; set; }

        public bool IsAuthor { get; set; }

        public bool IsAuthorized { get; set; }

        public List<Picture> TopicPictures { get; set; }

        public List<Comment> Comments { get; set; }

        public List<CommentModel> CommentModels { get; set; }
    }
}
