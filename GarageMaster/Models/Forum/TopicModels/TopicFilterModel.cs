﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.Forum.CommentModels;

namespace GarageMaster.Models.Forum.TopicModels
{
    public class TopicFilterModel
    {
        [Display(Name = "По автору темы")]
        public string Author { get; set; }

        [Display(Name = "По ключевому слову")]
        public string SearchKey { get; set; }

        [Display(Name = "Дата от")]
        public string DateFrom { get; set; }

        [Display(Name = "Дата до")]
        public string DateTo { get; set; }

        public List<TopicModel> Topics { get; set; }

        public TopicModel Topic { get; set; }

        public List<CommentModel> Comments { get; set; }

        [Display(Name = "Только мои темы")]
        public bool OnlyMyTopics { get; set; }

        public bool IsAuthorized { get; set; }

        public User CurrentUser { get; set; }

        public int? Page { get; set; }

        public TopicPageModel TopicPageModel { get; set; }
    }
}
