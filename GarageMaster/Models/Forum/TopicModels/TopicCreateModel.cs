﻿using System.ComponentModel.DataAnnotations;
using GarageMaster.Validation.Contracts;
using Microsoft.AspNetCore.Http;

namespace GarageMaster.Models.Forum.TopicModels
{
    public class TopicCreateModel : IValidated
    {
        [Required(ErrorMessage = "Поле \"Заголовок темы\" должно быть заполненно")]
        [Display(Name = "Заголовок темы")]
        public string TopicTitle { get; set; }

        [Required(ErrorMessage = "Поле \"Содержание темы\" должно быть заполненно")]
        [Display(Name = "Содержание темы")]
        public string TopicContent { get; set; }

        [Display(Name = "Добавить фотографии")]
        public IFormFileCollection Images { get; set; }
    }
}
