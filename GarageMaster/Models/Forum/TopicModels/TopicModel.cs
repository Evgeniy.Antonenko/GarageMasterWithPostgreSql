﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities.Pictures;

namespace GarageMaster.Models.Forum.TopicModels
{
    public class TopicModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Дата создания темы")]
        public DateTime TopicCreationDate { get; set; }

        [Display(Name = "Заголовок темы")]
        public string TopicTitle { get; set; }

        [Display(Name = "Содержание темы")]
        public string TopicContent { get; set; }

        public Guid AuthorId { get; set; }

        [Display(Name = "Автор темы")]
        public string Author { get; set; }

        [Display(Name = "Кол-во комментавиев")]
        public int CommentsQty { get; set; }

        [Display(Name = "Состояние темы")]
        public bool IsLocked { get; set; }

        public List<Picture> TopicPictures { get; set; }
    }
}
