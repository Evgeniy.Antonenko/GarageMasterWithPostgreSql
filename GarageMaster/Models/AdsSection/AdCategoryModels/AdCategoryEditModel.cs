﻿using System;
using System.ComponentModel.DataAnnotations;
using GarageMaster.Validation.Contracts;

namespace GarageMaster.Models.AdsSection.AdCategoryModels
{
    public class AdCategoryEditModel : IValidated
    {
        public Guid Id { get; set; }

        [Display(Name = "Название категории объявления")]
        [Required(ErrorMessage = "Поле \"Название категории объявления\" должно быть заполненно")]
        [MaxLength(150, ErrorMessage = "Максимальная длина 150 символов!")]
        [RegularExpression(@"^[ \-\ а-яА-Я]+$", ErrorMessage = "Используйте только буквы \"кириллицы\"!")]
        public string AdCategoryName { get; set; }
    }
}
