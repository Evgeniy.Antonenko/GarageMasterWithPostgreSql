﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.AdsSection.AdCategoryModels
{
    public class AdCategoryDeleteModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Название категории объявления")]
        public string AdCategoryName { get; set; }
    }
}
