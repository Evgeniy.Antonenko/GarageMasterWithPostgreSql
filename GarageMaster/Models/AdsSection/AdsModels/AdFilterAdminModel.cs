﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GarageMaster.Models.AdsSection.AdsModels
{
    public class AdFilterAdminModel
    {
        [Display(Name = "По категории объявления")]
        public Guid? AdCategoryId { get; set; }

        [Display(Name = "По автору объявления")]
        public string Author { get; set; }

        [Display(Name = "По ключевому слову")]
        public string SearchKey { get; set; }

        [Display(Name = "Дата от")]
        public string DateFrom { get; set; }

        [Display(Name = "Дата до")]
        public string DateTo { get; set; }

        public List<AdAdminModel> Ads { get; set; }

        public AdAdminModel Ad { get; set; }

        public SelectList AdCategoriesSelect { get; set; }

        public int? Page { get; set; }

        public AdPageModel AdPageModel { get; set; }
    }
}
