﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GarageMaster.Models.AdsSection.AdsModels
{
    public class AdFilterModel
    {
        [Display(Name = "По категории объявления")]
        public Guid? AdCategoryId { get; set; }

        [Display(Name = "По автору объявления")]
        public string Author { get; set; }

        [Display(Name = "По ключевому слову")]
        public string SearchKey { get; set; }

        [Display(Name = "Дата от")]
        public string DateFrom { get; set; }

        [Display(Name = "Дата до")]
        public string DateTo { get; set; }

        [Display(Name = "Только мои объявления")]
        public bool OnlyMyAds { get; set; }

        [Display(Name = "Объявления только с фотографиями")]
        public bool OnlyWithPhotos { get; set; }

        [Display(Name = "Объявления только без фотографиий")]
        public bool OnlyWithoutPhotos { get; set; }

        public bool IsAuthorized { get; set; }

        public User CurrentUser { get; set; }

        public List<AdModel> Ads { get; set; }

        public AdModel Ad { get; set; }

        public SelectList AdCategoriesSelect { get; set; }

        public int? Page { get; set; }

        public AdPageModel AdPageModel { get; set; }
    }
}
