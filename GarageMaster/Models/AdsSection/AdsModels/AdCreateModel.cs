﻿using System;
using System.ComponentModel.DataAnnotations;
using GarageMaster.Validation.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GarageMaster.Models.AdsSection.AdsModels
{
    public class AdCreateModel : IValidated
    {
        [Display(Name = "Заголовок объявления")]
        [Required(ErrorMessage = "Поле \"Заголовок объявления\" должно быть заполненно")]
        public string Title { get; set; }

        [Display(Name = "Содержание объявления")]
        [Required(ErrorMessage = "Поле \"Содержание объявления\" должно быть заполненно")]
        public string Content { get; set; }

        [Display(Name = "Категория объявления")]
        [Required(ErrorMessage = "Категория объявления должна быть выбрана")]
        public Guid AdCategoryId { get; set; }

        public SelectList AdCategoriesSelect { get; set; }

        [Display(Name = "Добавить фотографии")]
        public IFormFileCollection Images { get; set; }
    }
}
