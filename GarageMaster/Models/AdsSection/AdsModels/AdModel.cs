﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Pictures;

namespace GarageMaster.Models.AdsSection.AdsModels
{
    public class AdModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Заголовок объявления")]
        public string Title { get; set; }

        [Display(Name = "Содержание объявления")]
        public string Content { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime DateOfPublication { get; set; }

        [Display(Name = "Обновленная дата публикации ")]
        public DateTime PickUpDate { get; set; }

        [Display(Name = "Id автора")]
        public Guid UserId { get; set; }

        [Display(Name = "Автор")]
        public User User { get; set; }

        [Display(Name = "Id категории")]
        public Guid AdCategoryId { get; set; }

        [Display(Name = "Категория")]
        public AdCategory AdCategory { get; set; }

        public List<Picture> AdPictures { get; set; } 

        [Display(Name = "Кол-во добавленных фото")]
        public int QtyPhotos { get; set; }

        public bool IsAuthor { get; set; }
    }
}
