﻿using System;
using System.ComponentModel.DataAnnotations;
using GarageMaster.Validation.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GarageMaster.Models.AdsSection.AdsModels
{
    public class AdEditAdminModel : IValidated
    {
        public Guid Id { get; set; }

        [Display(Name = "Заголовок объявления")]
        [Required(ErrorMessage = "Поле \"Заголовок объявления\" должно быть заполненно")]
        public string Title { get; set; }

        [Display(Name = "Содержание объявления")]
        [Required(ErrorMessage = "Поле \"Содержание объявления\" должно быть заполненно")]
        public string Content { get; set; }

        [Display(Name = "Категория объявления")]
        [Required(ErrorMessage = "Категория объявления должна быть выбрана")]
        public Guid AdCategoryId { get; set; }

        public SelectList AdCategoriesSelect { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime DateOfPublication { get; set; }

        [Display(Name = "Обновленная дата публикации ")]
        public DateTime PickUpDate { get; set; }

        [Display(Name = "Id автора")]
        public Guid UserId { get; set; }

        [Display(Name = "Скинуть обновление даты публикации")]
        public bool DiscardPickUpDate { get; set; }
    }
}
