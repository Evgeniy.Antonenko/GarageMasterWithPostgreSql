﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GarageMaster.DAL.Entities.AdsSection;

namespace GarageMaster.Models.AdsSection.AdsModels
{
    public class AdDeleteModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Заголовок объявления")]
        public string Title { get; set; }

        [Display(Name = "Содержание объявления")]
        public string Content { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime DateOfPublication { get; set; }

        [Display(Name = "Кол-во добавленных фото")]
        public int QtyPhotos { get; set; }
    }
}
