﻿using System;

namespace GarageMaster.Models.AdsSection.AdsModels
{
    public class AdPageModel
    {
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }

        public AdPageModel(int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public bool HasPreviousPage => PageNumber > 1;

        public bool HasNextPage => PageNumber < TotalPages;
    }
}
