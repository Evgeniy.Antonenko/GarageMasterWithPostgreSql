﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.AccountModels
{
    public class UserDetailsModel
    {
        public UserDetailsModel()
        {
            Roles = new List<string>();
        }
        public Guid Id { get; set; }

        [Display(Name = "Email:")]
        public string Email { get; set; }

        [Display(Name = "Фамилия:")]
        public string Surname { get; set; }

        [Display(Name = "Имя:")]
        public string Name { get; set; }

        [Display(Name = "Псевдоним (Nickname):")]
        public string NickName { get; set; }

        [Display(Name = "MobilePhone:")]
        public string MobilePhone { get; set; }

        [Display(Name = "Фото (Аватар):")]
        public string Photo { get; set; }

        [Display(Name = "Дата регистрации:")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Состояние:")]
        public string IsActive { get; set; }

        [Display(Name = "Роли пользователя:")]
        public IList<string> Roles { get; set; }
    }
}
