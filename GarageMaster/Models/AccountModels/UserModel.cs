﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.AccountModels
{
    public class UserModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Псевдоним (Nickname)")]
        public string NickName { get; set; }

        [Display(Name = "MobilePhone")]
        public string MobilePhone { get; set; }

        [Display(Name = "Фото (Аватар)")]
        public string Photo { get; set; }

        [Display(Name = "Дата регистрации")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Состояние")]
        public bool IsActive { get; set; }
    }
}
