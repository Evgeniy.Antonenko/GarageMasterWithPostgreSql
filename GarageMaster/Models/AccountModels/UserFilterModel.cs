﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.AccountModels
{
    public class UserFilterModel
    {
        [Display(Name = "По e-mail")]
        public string SearchEmail { get; set; }

        [Display(Name = "По Фамилии, Имени или Псевдониму (Nickname)")]
        public string SearchSurname { get; set; }

        [Display(Name = "По № телефона")]
        public string SearchMobilePhone { get; set; }

        [Display(Name = "Дата регистрации от")]
        public string DateFrom { get; set; }

        [Display(Name = "Дата регистрации до")]
        public string DateTo { get; set; }

        public List<UserModel> Users { get; set; }

        public UserModel UserModel { get; set; }
    }
}
