﻿using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.AccountModels
{
    public class ChangePasswordAdminModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Введите новый пароль")]
        public string NewPassword { get; set; }


        [Required]
        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить новый пароль")]
        public string ConfirmNewPassword { get; set; }

        public string UserId { get; set; }
    }
}
