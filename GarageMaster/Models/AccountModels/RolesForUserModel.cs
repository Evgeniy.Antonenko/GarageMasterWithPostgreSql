﻿using System;

namespace GarageMaster.Models.AccountModels
{
    public class RolesForUserModel
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsSelected { get; set; }
    }
}
