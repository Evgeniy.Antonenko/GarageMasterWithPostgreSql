﻿using System.ComponentModel.DataAnnotations;
using GarageMaster.Validation.Contracts;
using Microsoft.AspNetCore.Http;

namespace GarageMaster.Models.AccountModels
{
    public class RegisterModel : IValidated
    {
        [Required(ErrorMessage = "Поле \"Email\" должно быть заполненно")]
        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Поле \"Фамилия\" должно быть заполненно")]
        [Display(Name = "Фамилия")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        [RegularExpression(@"^[а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Surname { get; set; }


        [Required(ErrorMessage = "Поле \"Имя\" должно быть заполненно")]
        [Display(Name = "Имя")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        [RegularExpression(@"^[а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Поле \"Псевдоним (Nickname)\" должно быть заполненно")]
        [Display(Name = "Псевдоним (Nickname)")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        public string NickName { get; set; }


        [Required(ErrorMessage = "Поле \"MobilePhone\" должно быть заполненно")]
        [Display(Name = "MobilePhone")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone")]
        public string MobilePhone { get; set; }


        [Required(ErrorMessage = "Поле \"Пароль\" должно быть заполненно")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }


        [Required(ErrorMessage = "Поле \"Подтвердить пароль\" должно быть заполненно")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }

        
        [Display(Name = "Я прочитал и соглашаюсь с правилами и условиями пользования данным ресурсом")]
        [Required(ErrorMessage = "Вы должны подтверить свое согласие с правилами и условиями пользования данного ресурса")]
        public bool AgreementRules { get; set; }

        public IFormFile PhotoGet { get; set; }
    }
}
