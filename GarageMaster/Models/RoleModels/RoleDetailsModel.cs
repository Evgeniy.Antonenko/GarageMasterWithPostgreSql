﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.RoleModels
{
    public class RoleDetailsModel
    {
        public RoleDetailsModel()
        {
            Users = new List<string>();
        }

        public Guid Id { get; set; }

        [Display(Name = "Название роли:")]
        public string Name { get; set; }

        [Display(Name = "Пользователи роли:")]
        public List<string> Users { get; set; }
    }
}
