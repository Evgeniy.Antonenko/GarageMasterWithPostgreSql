﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GarageMaster.Models.RoleModels
{
    public class RoleModel
    {
        [Display(Name = "ID роли")]
        public Guid Id { get; set; }

        [Display(Name = "Название Роли")]
        public string Name { get; set; }
    }
}
