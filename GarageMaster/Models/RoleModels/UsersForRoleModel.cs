﻿using System;

namespace GarageMaster.Models.RoleModels
{
    public class UsersForRoleModel
    {
        public Guid RoleId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public bool IsSelected { get; set; }
    }
}
