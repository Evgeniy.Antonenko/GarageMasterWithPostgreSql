﻿using System.Collections.Generic;

namespace GarageMaster.Models.RoleModels
{
    public class RolesModel
    {
        public List<RoleModel> Roles { get; set; }

        public RoleModel RoleModel { get; set; }
    }
}
