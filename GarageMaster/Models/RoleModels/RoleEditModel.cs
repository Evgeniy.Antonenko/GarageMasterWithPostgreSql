﻿using System;
using System.ComponentModel.DataAnnotations;
using GarageMaster.Validation.Contracts;

namespace GarageMaster.Models.RoleModels
{
    public class RoleEditModel : IValidated
    {
        [Display(Name = "ID роли")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Поле \"Название роли\" должно быть заполненно")]
        [Display(Name = "Название роли")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        public string Name { get; set; }
    }
}
