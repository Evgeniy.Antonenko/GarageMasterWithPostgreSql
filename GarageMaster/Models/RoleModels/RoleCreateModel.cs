﻿using System.ComponentModel.DataAnnotations;
using GarageMaster.Validation.Contracts;


namespace GarageMaster.Models.RoleModels
{
    public class RoleCreateModel : IValidated
    {
        [Required(ErrorMessage = "Поле \"Название роли\" должно быть заполненно")]
        [Display(Name = "Название роли")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        public string Name { get; set; }
    }
}
