﻿using GarageMaster.DAL;
using GarageMaster.Models.AdsSection.AdsModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.AdsSection.Ads
{
    public class AdEditAdminValidator : Validator<AdEditAdminModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdEditAdminValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateTitleInDb(),
                ValidateContentInDb()
            });
        }

        private ValidationItem ValidateTitleInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ads = unitOfWork.Ads.GetAdWithoutById(validatedModel.Id);

                foreach (var ad in ads)
                {
                    if (ad.Title == validatedModel.Title) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Объявление с таким заголовком {validatedModel.Title} уже существует"
                };
            }
        }

        private ValidationItem ValidateContentInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ads = unitOfWork.Ads.GetAdWithoutById(validatedModel.Id);

                foreach (var ad in ads)
                {
                    if (ad.Content == validatedModel.Content) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Объявление с таким содержанием {validatedModel.Content} уже существует"
                };
            }
        }
    }
}
