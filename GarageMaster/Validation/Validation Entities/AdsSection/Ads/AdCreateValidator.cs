﻿using GarageMaster.DAL;
using GarageMaster.Models.AdsSection.AdsModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.AdsSection.Ads
{
    public class AdCreateValidator : Validator<AdCreateModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdCreateValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateTitleInDb(),
                ValidateContentInDb()
            });
        }

        private ValidationItem ValidateTitleInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetAdByTitle(validatedModel.Title);

                return new ValidationItem()
                {
                    Predicate = ad != null,
                    ErrorMessage = $"Объявление с таким заголовком {validatedModel.Title} уже существует"
                };
            }
        }

        private ValidationItem ValidateContentInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetAdByContent(validatedModel.Title);

                return new ValidationItem()
                {
                    Predicate = ad != null,
                    ErrorMessage = $"Объявление с таким содержанием {validatedModel.Content} уже существует"
                };
            }
        }
    }
}
