﻿using GarageMaster.DAL;
using GarageMaster.Models.AdsSection.AdCategoryModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.AdsSection.AdCatagories
{
    public class AdCategoryCreateValidator : Validator<AdCategoryCreateModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdCategoryCreateValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateTitleInDb()
            });
        }

        private ValidationItem ValidateTitleInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var directory = unitOfWork.AdCategories.GetByAdCategoryName(validatedModel.AdCategoryName);

                return new ValidationItem()
                {
                    Predicate = directory != null,
                    ErrorMessage = $"Категория объявления с таким названием {validatedModel.AdCategoryName} уже существует"
                };
            }
        }
    }
}
