﻿using GarageMaster.DAL;
using GarageMaster.Models.AdsSection.AdCategoryModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.AdsSection.AdCatagories
{
    public class AdCategoryEditValidator : Validator<AdCategoryEditModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdCategoryEditValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateNameInDb()
            });
        }

        private ValidationItem ValidateNameInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var directories = unitOfWork.AdCategories.GetAdCategoryWithoutById(validatedModel.Id);

                foreach (var directory in directories)
                {
                    if (directory.AdCategoryName == validatedModel.AdCategoryName) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Категория объявления с таким названием {validatedModel.AdCategoryName} уже существует"
                };
            }
        }
    }
}
