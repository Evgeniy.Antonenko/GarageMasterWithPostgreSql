﻿using GarageMaster.DAL;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.Forum
{
    public class TopicEditValidator : Validator<TopicEditModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public TopicEditValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateTitleInDb()
            });
        }

        private ValidationItem ValidateTitleInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topics = unitOfWork.Topics.GetTopicsWithoutById(validatedModel.Id);

                foreach (var topic in topics)
                {
                    if (topic.TopicTitle == validatedModel.TopicTitle) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Тема форума с таким названием {validatedModel.TopicTitle} уже существует"
                };
            }
        }
    }
}
