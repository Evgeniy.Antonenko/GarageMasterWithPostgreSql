﻿using System.Linq;
using GarageMaster.DAL;
using GarageMaster.Models.Forum.CommentModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.Forum
{
    public class CommentEditValidator : Validator<CommentEditModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CommentEditValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateCommentContentInDb()
            });
        }

        private ValidationItem ValidateCommentContentInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var commentsInTopic = unitOfWork.Comments.GetCommentsWithAuthorsAndTopicsByTopicId(validatedModel.TopicId).ToList();
                var comments = commentsInTopic.Where(c => c.Id != validatedModel.Id);

                foreach (var comment in comments)
                {
                    if (comment.CommentContent == validatedModel.CommentContent) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Такой комментарий {validatedModel.CommentContent} уже существует"
                };
            }
        }
    }
}
