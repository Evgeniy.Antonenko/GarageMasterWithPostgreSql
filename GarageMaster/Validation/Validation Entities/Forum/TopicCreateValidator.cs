﻿using GarageMaster.DAL;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.Forum
{
    public class TopicCreateValidator : Validator<TopicCreateModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public TopicCreateValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateTitleInDb()
            });
        }

        private ValidationItem ValidateTitleInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topic = unitOfWork.Topics.GetByTopicTitle(validatedModel.TopicTitle);

                return new ValidationItem()
                {
                    Predicate = topic != null,
                    ErrorMessage = $"Тема форума с таким названием {validatedModel.TopicTitle} уже существует"
                };
            }
        }
    }
}
