﻿using System.Linq;
using GarageMaster.DAL;
using GarageMaster.Models.Forum.CommentModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.Forum
{
    public class CommentCreateValidator : Validator<CommentCreateModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CommentCreateValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateCommentContentInDb()
            });
        }

        private ValidationItem ValidateCommentContentInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var commentsInTopic = unitOfWork.Comments.GetCommentsWithAuthorsAndTopicsByTopicId(validatedModel.TopicId);
                var comment = commentsInTopic.FirstOrDefault(c => c.CommentContent == validatedModel.CommentContent);

                return new ValidationItem()
                {
                    Predicate = comment != null,
                    ErrorMessage = $"Такой комментарий {validatedModel.CommentContent} уже существует"
                };
            }
        }
    }
}
