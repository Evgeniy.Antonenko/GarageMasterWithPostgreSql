﻿using GarageMaster.DAL;
using GarageMaster.Models.AccountModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.Account
{
    public class RegisterValidator : Validator<RegisterModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RegisterValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                new ValidationItem()
                {
                    Predicate = validatedModel.AgreementRules == false,
                    ErrorMessage = "Вы должны подтверить свое согласие с правилами и условиями пользования данного ресурса"
                },
                ValidateUserNameInDb(),
                ValidateUserNickNameInDb(),
                ValidateMobilePhoneInDb()
            });
        }

        private ValidationItem ValidateUserNameInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = unitOfWork.User.GetByUserName(validatedModel.Email);

                return new ValidationItem()
                {
                    Predicate = user != null,
                    ErrorMessage = $"Пользователь с таким e-mail {validatedModel.Email} уже существует"
                };
            }
        }

        private ValidationItem ValidateUserNickNameInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = unitOfWork.User.GetByUserNickName(validatedModel.NickName);

                return new ValidationItem()
                {
                    Predicate = user != null,
                    ErrorMessage = $"Пользователь с таким \"Псевдонимом (Nickname)\" {validatedModel.NickName} уже существует"
                };
            }
        }

        private ValidationItem ValidateMobilePhoneInDb()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var user = unitOfWork.User.GetByMobilePhone(validatedModel.MobilePhone);

                return new ValidationItem()
                {
                    Predicate = user != null,
                    ErrorMessage =
                        $"Пользователь с таким № мобильного телефона {validatedModel.MobilePhone} уже существует"
                };
            }
        }
    }
}
