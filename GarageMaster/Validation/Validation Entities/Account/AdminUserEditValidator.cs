﻿using GarageMaster.DAL;
using GarageMaster.Models.AccountModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.Account
{
    public class AdminUserEditValidator : Validator<AdminUserEditModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdminUserEditValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateNickNameInDb(),
                ValidateMobilePhoneInDb()
            });
        }

        private ValidationItem ValidateNickNameInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var users = unitOfWork.User.GetUsersWithoutById(validatedModel.Id);

                foreach (var user in users)
                {
                    if (user.NickName == validatedModel.NickName) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Пользователь с таким \"Псевдонимом (Nickname)\" {validatedModel.NickName} уже существует"
                };
            }
        }

        private ValidationItem ValidateMobilePhoneInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var users = unitOfWork.User.GetUsersWithoutById(validatedModel.Id);

                foreach (var user in users)
                {
                    if (user.MobilePhone == validatedModel.MobilePhone) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Пользователь с таким № мобильного телефона {validatedModel.MobilePhone} уже существует"
                };
            }
        }
    }
}
