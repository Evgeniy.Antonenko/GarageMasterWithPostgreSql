﻿using System.Linq;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.RoleModels;
using GarageMaster.Validation.Models;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.Validation.Validation_Entities.Roles
{
    public class RoleCreateValidator : Validator<RoleCreateModel>
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RoleCreateValidator(RoleManager<Role> roleManager, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _roleManager = roleManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateRoleNameInDb()
            });
        }

        private ValidationItem ValidateRoleNameInDb()
        {
            var role = _roleManager.Roles.FirstOrDefault(r => r.Name == validatedModel.Name);

            return new ValidationItem()
            {
                Predicate = role != null,
                ErrorMessage = $"Роль с таким названием {validatedModel.Name} уже существует"
            };
        }
    }
}
