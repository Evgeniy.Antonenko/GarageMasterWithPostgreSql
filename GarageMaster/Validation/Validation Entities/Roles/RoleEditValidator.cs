﻿using GarageMaster.DAL;
using GarageMaster.Models.RoleModels;
using GarageMaster.Validation.Models;

namespace GarageMaster.Validation.Validation_Entities.Roles
{
    public class RoleEditValidator : Validator<RoleEditModel>
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RoleEditValidator(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        protected override void SetValidationItems()
        {
            validationItems.AddRange(new[]
            {
                ValidateRoleNameInDb()
            });
        }

        private ValidationItem ValidateRoleNameInDb()
        {
            var isBool = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var roles = unitOfWork.Role.GetRolesWithoutById(validatedModel.Id);

                foreach (var role in roles)
                {
                    if (role.Name == validatedModel.Name) isBool = false;
                }

                return new ValidationItem()
                {
                    Predicate = isBool == false,
                    ErrorMessage = $"Роль с таким названием {validatedModel.Name} уже существует"
                };
            }
        }
    }
}
