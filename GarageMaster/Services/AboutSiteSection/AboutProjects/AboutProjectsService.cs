﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.Models.AboutSiteSection.AboutProjectModels;
using GarageMaster.Services.AboutSiteSection.AboutProjects.Contracts;

namespace GarageMaster.Services.AboutSiteSection.AboutProjects
{
    public class AboutProjectsService : IAboutProjectsService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AboutProjectsService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public AboutProjectByAdminModel GetAboutProjectByAdminModel(AboutProjectByAdminModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProjectCreated = unitOfWork.AboutProjects.GetAll().Any();
                if (aboutProjectCreated)
                {
                    var aboutProject = unitOfWork.AboutProjects.GetAll().First();
                    model = Mapper.Map<AboutProjectByAdminModel>(aboutProject);
                }
                else
                {
                    model.Title = "No data to display";
                    model.Content = "No data to display";
                    model.IsCreated = false;
                }
            }
            return model;
        }

        public AboutProjectModel GetAboutProject()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = unitOfWork.AboutProjects.GetAll().First();
                var aboutProjectModel = Mapper.Map<AboutProjectModel>(aboutProject);
                return aboutProjectModel;
            }
        }

        public void CreateAboutProject(AboutProjectCreateModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = Mapper.Map<AboutProject>(model);

                unitOfWork.AboutProjects.Create(aboutProject);
            }
        }

        public AboutProjectEditModel GetAboutProjectForEdit()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = unitOfWork.AboutProjects.GetAll().First();
                return Mapper.Map<AboutProjectEditModel>(aboutProject);
            }
        }

        public void EditAboutProject(AboutProjectEditModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var aboutProject = Mapper.Map<AboutProject>(model);

                unitOfWork.AboutProjects.Update(aboutProject);
            }
        }
    }
}
