﻿using GarageMaster.Models.AboutSiteSection.AboutProjectModels;

namespace GarageMaster.Services.AboutSiteSection.AboutProjects.Contracts
{
    public interface IAboutProjectsService
    {
        void EditAboutProject(AboutProjectEditModel model);
        AboutProjectEditModel GetAboutProjectForEdit();
        void CreateAboutProject(AboutProjectCreateModel model);
        AboutProjectModel GetAboutProject();
        AboutProjectByAdminModel GetAboutProjectByAdminModel(AboutProjectByAdminModel model);
    }
}
