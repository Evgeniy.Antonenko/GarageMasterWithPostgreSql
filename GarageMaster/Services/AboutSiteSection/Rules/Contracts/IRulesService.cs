﻿using GarageMaster.Models.AboutSiteSection.RulesModels;

namespace GarageMaster.Services.AboutSiteSection.Rules.Contracts
{
    public interface IRulesService
    {
        RuleByAdminModel GetRuleByAdminModel(RuleByAdminModel model);
        void EditRule(RuleEditModel model);
        RuleEditModel GetRuleForEdit();
        void CreateRule(RuleCreateModel model);
        RuleModel GetRule();
    }
}
