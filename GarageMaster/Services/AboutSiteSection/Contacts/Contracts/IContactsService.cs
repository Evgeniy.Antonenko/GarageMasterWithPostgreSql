﻿using GarageMaster.Models.AboutSiteSection.ContactsModels;

namespace GarageMaster.Services.AboutSiteSection.Contacts.Contracts
{
    public interface IContactsService
    {
        void EditContact(ContactEditModel model);
        ContactEditModel GetContactForEdit();
        void CreateContact(ContactCreateModel model);
        ContactModel GetContact();
        ContactByAdminModel GetContactByAdminModel(ContactByAdminModel model);
    }
}
