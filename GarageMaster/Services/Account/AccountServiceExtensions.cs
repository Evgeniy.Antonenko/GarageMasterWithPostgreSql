﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities;

namespace GarageMaster.Services.Account
{
    public static class AccountServiceExtensions
    {
        public static IEnumerable<User> BySearchEMail(this IEnumerable<User> users, string searchEMail)
        {
            if (!string.IsNullOrWhiteSpace(searchEMail))
                users = users.Where(u => u.Email.Contains(searchEMail));

            return users;
        }

        public static IEnumerable<User> BySearchSurnameOrNameOrNickName(this IEnumerable<User> users, string searchSurname)
        {
            if (!string.IsNullOrWhiteSpace(searchSurname))
                users = users.Where(u => u.Surname.Contains(searchSurname) || u.Name.Contains(searchSurname) || u.NickName.Contains(searchSurname));

            return users;
        }

        public static IEnumerable<User> BySearchMobilePhone(this IEnumerable<User> users, string searchMobilePhone)
        {
            if (!string.IsNullOrWhiteSpace(searchMobilePhone))
                users = users.Where(u => u.MobilePhone.Contains(searchMobilePhone));

            return users;
        }

        public static IEnumerable<User> ByDateFrom(this IEnumerable<User> users, string dateFrom)
        {
            if (!string.IsNullOrWhiteSpace(dateFrom))
                users = users.Where(u => u.CreatedOn >= Convert.ToDateTime(dateFrom));

            return users;
        }

        public static IEnumerable<User> ByDateTo(this IEnumerable<User> users, string dateTo)
        {
            if (!string.IsNullOrWhiteSpace(dateTo))
                users = users.Where(u => u.CreatedOn <= Convert.ToDateTime(dateTo));

            return users;
        }
    }
}
