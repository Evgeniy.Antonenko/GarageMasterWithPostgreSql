﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AccountModels;
using GarageMaster.Validation;

namespace GarageMaster.Services.Account.Contracts
{
    public interface IAccountService
    {
        Task UpdateRolesForUserAsync(List<RolesForUserModel> model, Guid userId);
        Task<List<RolesForUserModel>> GetRolesForUserAsync(Guid id);
        Task<ValidationResult> AdminEditUser(AdminUserEditModel model);
        Task<AdminUserEditModel> GetUserForAdminEditModel(Guid id);
        Task<UserDetailsModel> GetUserModelForAdmin(Guid id);
        UserFilterModel GetListUsersForAdmin(UserFilterModel model);
        RegisterModel GetRegisterModel();
        Task<ValidationResult> CreateUser(RegisterModel model);
        Task<User> GetUserToRegisterAsync(RegisterModel model);
        bool CheckUserInDb();
        UserModel GetUserModel(User user);
        UserEditModel GetUserEditModel(User user);
        Task<ValidationResult> EditUser(UserEditModel model, User user);
    }
}
