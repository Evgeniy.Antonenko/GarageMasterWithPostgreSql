﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AccountModels;
using GarageMaster.Services.Account.Contracts;
using GarageMaster.Validation;
using GarageMaster.Validation.Validation_Entities.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.Services.Account
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;
        private readonly RegisterValidator _registerValidator;
        private readonly UserEditValidator _userEditValidator;
        private readonly AdminUserEditValidator _adminUserEditValidator;


        public AccountService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager, RegisterValidator registerValidator, UserEditValidator userEditValidator, AdminUserEditValidator adminUserEditValidator)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
            _registerValidator = registerValidator;
            _userEditValidator = userEditValidator;
            _adminUserEditValidator = adminUserEditValidator;
        }

        public async Task<List<RolesForUserModel>> GetRolesForUserAsync(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());

            var model = new List<RolesForUserModel>();

            List<Role> roles = new List<Role>();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                roles = unitOfWork.Role.GetAll().ToList();
            }

            foreach (var role in roles)
            {
                var rolesForUserModel = Mapper.Map<RolesForUserModel>(role);

                rolesForUserModel.UserId = user.Id;

                rolesForUserModel.IsSelected = await _userManager.IsInRoleAsync(user, role.Name);

                model.Add(rolesForUserModel);
            }

            return model;
        }

        public async Task UpdateRolesForUserAsync(List<RolesForUserModel> model, Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());

            var roles = await _userManager.GetRolesAsync(user);
            await _userManager.RemoveFromRolesAsync(user, roles);

            await _userManager
                .AddToRolesAsync(user, model.Where(x => x.IsSelected).Select(y => y.RoleName));
        }

        
        public async Task<ValidationResult> AdminEditUser(AdminUserEditModel model)
        {
            var validationResult = _adminUserEditValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                User user = await _userManager.FindByIdAsync(model.Id.ToString());
                user = GetUserForAdminEdit(model, user);
                await _userManager.UpdateAsync(user);
            }
            return validationResult;
        }

        public async Task<AdminUserEditModel> GetUserForAdminEditModel(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            var admiUserEditModel = Mapper.Map<AdminUserEditModel>(user);
            
            return admiUserEditModel;
        }

        public async Task<UserDetailsModel> GetUserModelForAdmin(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            var userRoles = await _userManager.GetRolesAsync(user);
            UserDetailsModel userDetailsModel = Mapper.Map<UserDetailsModel>(user);
            if (user.IsActive == true)
            {
                userDetailsModel.IsActive = "IsActive";
            }
            else
            {
                userDetailsModel.IsActive = "IsNotActive";
            }

            userDetailsModel.Roles = userRoles;
            return userDetailsModel;
        }

        public UserFilterModel GetListUsersForAdmin(UserFilterModel model)
        {
            IEnumerable<User> users = _userManager.Users.ToList();
            users = users
                .BySearchEMail(model.SearchEmail)
                .BySearchSurnameOrNameOrNickName(model.SearchSurname)
                .BySearchMobilePhone(model.SearchMobilePhone)
                .ByDateFrom(model.DateFrom)
                .ByDateTo(model.DateTo);

            List<UserModel> userModels = Mapper.Map<List<UserModel>>(users);


            model.Users = userModels;

            return model;
        }

        public async Task<ValidationResult> EditUser(UserEditModel model, User user)
        {
            var validationResult = _userEditValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                user = await GetUserForEdit(model, user);
                await _userManager.UpdateAsync(user);
            }
            return validationResult;
        }

        public async Task<User> GetUserForEdit(UserEditModel model, User user)
        {
            user = Mapper.Map(model, user);
            if (model.PhotoEdit != null & model.RemovePhoto == false)
            {
                if (user.Photo != @"/assets/empty_avatar.jpg")
                {
                    string path = Directory.GetCurrentDirectory() + "/wwwroot" + user.Photo;
                    File.Delete(path);
                }
                user.Photo = await UploadImage(model.PhotoEdit);
            }
            if (model.PhotoEdit == null & model.RemovePhoto == true)
            {
                if (user.Photo != @"/assets/empty_avatar.jpg")
                {
                    string path = Directory.GetCurrentDirectory() + "/wwwroot" + user.Photo;
                    File.Delete(path);
                    user.Photo = @"/assets/empty_avatar.jpg";
                }
            }
            return user;
        }

        public RegisterModel GetRegisterModel()
        {
            return new RegisterModel { };
        }

        
        public UserEditModel GetUserEditModel(User user)
        {
            UserEditModel userEditModel = Mapper.Map<UserEditModel>(user);

            return userEditModel;
        }

        public UserModel GetUserModel(User user)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                UserModel userModel = Mapper.Map<UserModel>(user);

                return userModel;
            }
        }

        public async Task<ValidationResult> CreateUser(RegisterModel model)
        {
            var validationResult = _registerValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                var isBool = CheckUserInDb();
                var user = await GetUserToRegisterAsync(model);
                await _userManager.CreateAsync(user, model.Password);
                if (isBool == false)
                    await _userManager.AddToRoleAsync(user, "admin");
                else
                    await _userManager.AddToRoleAsync(user, "user");
            }
            return validationResult;
        }


        public async Task<User> GetUserToRegisterAsync(RegisterModel model)
        {
            User user = Mapper.Map<User>(model);
            user.UserName = model.Email;
            user.PhoneNumber = model.MobilePhone;
            user.CreatedOn = DateTime.Now;
            if (model.PhotoGet != null)
            {
                user.Photo = await UploadImage(model.PhotoGet);
            }
            return user;
        }

        public bool CheckUserInDb()
        {
            var isCreate = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var users = unitOfWork.User.GetAll().ToList().Count;
                if (users == 0)
                {
                    isCreate = false;
                }
                return isCreate;
            }
        }

        private async Task<string> UploadImage(IFormFile image)
        {
            string fileName = $"{Guid.NewGuid()}_{image.FileName}";
            string path = @"wwwroot";
            string subpath = @"uploads\profile_images";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string test = dirInfo.CreateSubdirectory(subpath).ToString();
            test = $@"wwwroot/uploads/profile_images/{fileName}";
            using (var stream = System.IO.File.Create(test))
            {
                await image.CopyToAsync(stream);
            }
            return $@"/uploads/profile_images/{fileName}";
        }

        private User GetUserForAdminEdit(AdminUserEditModel model, User user)
        {
            user = Mapper.Map(model, user);
           
            return user;
        }
    }
}
