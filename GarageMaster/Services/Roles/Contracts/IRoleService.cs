﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GarageMaster.Models.RoleModels;
using GarageMaster.Validation;

namespace GarageMaster.Services.Roles.Contracts
{
    public interface IRoleService
    {
        Task<RoleDetailsModel> GetRoleForEditAsync(Guid id);
        RolesModel GetListRolesForAdmin(RolesModel model);
        RoleCreateModel GetRoleCreateModel();
        Task<ValidationResult> CreateRole(RoleCreateModel model);
        Task<RoleEditModel> GetRoleEditModelAsync(Guid id);
        Task<ValidationResult> EditRoleAdminAsync(RoleEditModel model);
        Task<List<UsersForRoleModel>> GetUsersForRoleAsync(Guid id);
        Task UpdateUsersForRoleAsync(List<UsersForRoleModel> model, Guid roleId);
    }
}
