﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.RoleModels;
using GarageMaster.Services.Roles.Contracts;
using GarageMaster.Validation;
using GarageMaster.Validation.Validation_Entities.Roles;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.Services.Roles
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly RoleCreateValidator _roleCreateValidator;
        private readonly RoleEditValidator _roleEditValidator;

        public RoleService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager, RoleManager<Role> roleManager, RoleCreateValidator roleCreateValidator, RoleEditValidator roleEditValidator)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
            _roleManager = roleManager;
            _roleCreateValidator = roleCreateValidator;
            _roleEditValidator = roleEditValidator;
        }

        public RolesModel GetListRolesForAdmin(RolesModel model)
        {
            IEnumerable<Role> roles = _roleManager.Roles.ToList();

            List<RoleModel> roleModels = Mapper.Map<List<RoleModel>>(roles);

            model.Roles = roleModels;

            return model;
        }

        public RoleCreateModel GetRoleCreateModel()
        {
            return new RoleCreateModel { };
        }

        public async Task<ValidationResult> CreateRole(RoleCreateModel model)
        {
            model.Name = Regex.Replace(model.Name, @"\s+", " ").Trim();
            var validationResult = _roleCreateValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                var role = Mapper.Map<Role>(model);
                await _roleManager.CreateAsync(role);
            }
            return validationResult;
        }

        public async Task<RoleDetailsModel> GetRoleForEditAsync(Guid id)
        {
            var role = await _roleManager.FindByIdAsync(id.ToString());

            var model = new RoleDetailsModel { Id = role.Id, Name = role.Name };

            foreach (var user in _userManager.Users.ToList())
            {
                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    model.Users.Add(user.UserName);
                }
            }

            return model;
        }

        public async Task<RoleEditModel> GetRoleEditModelAsync(Guid id)
        {
            var role = await _roleManager.FindByIdAsync(id.ToString());
            var roleEditModel = Mapper.Map<RoleEditModel>(role);

            return roleEditModel;
        }

        public async Task<ValidationResult> EditRoleAdminAsync(RoleEditModel model)
        {
            var validationResult = _roleEditValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                Role role = await _roleManager.FindByIdAsync(model.Id.ToString());
                role = Mapper.Map(model, role);
                await _roleManager.UpdateAsync(role);
            }
            return validationResult;
        }

        public async Task<List<UsersForRoleModel>> GetUsersForRoleAsync(Guid id)
        {
            var role = await _roleManager.FindByIdAsync(id.ToString());

            var model = new List<UsersForRoleModel>();

            List<User> users = new List<User>();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                users = unitOfWork.User.GetAll().ToList();
            }

            foreach (var user in users)
            {
                var usersForRoleModel = Mapper.Map<UsersForRoleModel>(user);

                usersForRoleModel.RoleId = role.Id;

                usersForRoleModel.IsSelected = await _userManager.IsInRoleAsync(user, role.Name);

                model.Add(usersForRoleModel);
            }

            return model;
        }

        public async Task UpdateUsersForRoleAsync(List<UsersForRoleModel> model, Guid roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId.ToString());

            for (int i = 0; i < model.Count; i++)
            {
                var user = await _userManager.FindByIdAsync(model[i].UserId.ToString());


                if (model[i].IsSelected && !(await _userManager.IsInRoleAsync(user, role.Name)))
                {
                    await _userManager.AddToRoleAsync(user, role.Name);
                }
                else if (!model[i].IsSelected && (await _userManager.IsInRoleAsync(user, role.Name)))
                {
                    await _userManager.RemoveFromRoleAsync(user, role.Name);
                }
            }
        }
    }
}
