﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.Models.AdsSection.AdCategoryModels;
using GarageMaster.Services.AdsSection.AdCategories.Contracts;
using GarageMaster.Validation;
using GarageMaster.Validation.Validation_Entities.AdsSection.AdCatagories;
using Microsoft.AspNetCore.Mvc;

namespace GarageMaster.Services.AdsSection.AdCategories
{
    public class AdCategoryService : IAdCategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly AdCategoryCreateValidator _adCategoryCreateValidator;
        private readonly AdCategoryEditValidator _adCategoryEditValidator;

        public AdCategoryService(IUnitOfWorkFactory unitOfWorkFactory, AdCategoryCreateValidator adCategoryCreateValidator, AdCategoryEditValidator adCategoryEditValidator)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (adCategoryCreateValidator == null)
                throw new ArgumentNullException(nameof(adCategoryCreateValidator));
            if (adCategoryEditValidator == null)
                throw new ArgumentNullException(nameof(adCategoryEditValidator));

            _unitOfWorkFactory = unitOfWorkFactory;
            _adCategoryCreateValidator = adCategoryCreateValidator;
            _adCategoryEditValidator = adCategoryEditValidator;
        }
        
        public List<AdCategoryModel> GetAdCategoriesList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var adCategories = unitOfWork.AdCategories.GetAll().ToList();
                return Mapper.Map<List<AdCategoryModel>>(adCategories);
            }
        }

        public ValidationResult CreateAdCategory(AdCategoryCreateModel model)
        {
            model.AdCategoryName = Regex.Replace(model.AdCategoryName, @"\s+", " ").Trim();
            var validationResult = _adCategoryCreateValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var adCategory = Mapper.Map<AdCategory>(model);
                    unitOfWork.AdCategories.Create(adCategory);
                }
            }
            return validationResult;
        }

        public AdCategoryEditModel GetAdCategoryById(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var adCategory = unitOfWork.AdCategories.GetById(id);
                var model = Mapper.Map<AdCategoryEditModel>(adCategory);

                return model;
            }
        }

        public ValidationResult EditAdCategory(AdCategoryEditModel model)
        {
            model.AdCategoryName = Regex.Replace(model.AdCategoryName, @"\s+", " ").Trim();
            var validationResult = _adCategoryEditValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var adCategory = Mapper.Map<AdCategory>(model);
                    unitOfWork.AdCategories.Update(adCategory);
                }
            }
            return validationResult;
        }

        public AdCategoryDeleteModel GetAdCategoryByIdForDelete(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var adCategory = unitOfWork.AdCategories.GetById(id);
                var model = Mapper.Map<AdCategoryDeleteModel>(adCategory);

                return model;
            }
        }

        public void DeleteAdCategory(AdCategoryDeleteModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var adCategory = unitOfWork.AdCategories.GetById(model.Id);
                unitOfWork.AdCategories.Remove(adCategory);
            }
        }
    }
}
