﻿using System;
using System.Collections.Generic;
using GarageMaster.Models.AdsSection.AdCategoryModels;
using GarageMaster.Validation;

namespace GarageMaster.Services.AdsSection.AdCategories.Contracts
{
    public interface IAdCategoryService
    {
        void DeleteAdCategory(AdCategoryDeleteModel model);
        AdCategoryDeleteModel GetAdCategoryByIdForDelete(Guid id);
        ValidationResult EditAdCategory(AdCategoryEditModel model);
        AdCategoryEditModel GetAdCategoryById(Guid id);
        ValidationResult CreateAdCategory(AdCategoryCreateModel model);
        List<AdCategoryModel> GetAdCategoriesList();
    }
}
