﻿using System;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.AdsSection.AdsModels;
using GarageMaster.Validation;

namespace GarageMaster.Services.AdsSection.Ads.Contracts
{
    public interface IAdsService
    {
        void DeleteAd(AdDeleteModel model);
        AdDeleteModel GetAdDeleteModel(Guid id);
        void MakePickUpDate(Guid adId);
        AdModel GetAdById(Guid id, User currentUser);
        ValidationResult EditAdAdmin(AdEditAdminModel model);
        ValidationResult EditAd(AdEditModel model, Guid currentUserId);
        AdEditAdminModel GetAdEditAdminModel(Guid id);
        AdEditModel GetAdEditModel(Guid id);
        ValidationResult CreateAd(AdCreateModel model, User currentUser);
        AdCreateModel GetAdCreateModel();
        AdFilterAdminModel GetAdFilterAdminModel(AdFilterAdminModel model);
        AdFilterModel GetAdFilterModel(AdFilterModel model);
    }
}
