﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.Models.AdsSection.AdsModels;

namespace GarageMaster.Services.AdsSection.Ads
{
    public static class AdServiceExtensions
    {
        public static IEnumerable<Ad> ByAdCategoryId(this IEnumerable<Ad> ads, UnitOfWork unitOfWork, Guid? adCategoryId)
        {
            if (adCategoryId.HasValue)
            {
                var adCategory = unitOfWork.AdCategories.GetById(adCategoryId.Value);
                if (adCategory == null)
                    throw new ArgumentOutOfRangeException(nameof(adCategoryId), $"No ad catagory with Id {adCategoryId}");
                return ads.Where(a => a.AdCategoryId == adCategoryId);
            }
            return ads;
        }

        public static IEnumerable<Ad> BySearchKey(this IEnumerable<Ad> ads, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                ads = ads.Where(t => t.Title.Contains(searchKey) || t.Content.Contains(searchKey));

            return ads;
        }

        public static IEnumerable<Ad> ByAuthorName(this IEnumerable<Ad> ads, string authorName)
        {
            if (!string.IsNullOrWhiteSpace(authorName))
                ads = ads.Where(t => t.User.NickName.Contains(authorName));

            return ads;
        }

        public static IEnumerable<Ad> ByDateFrom(this IEnumerable<Ad> ads, string dateFrom)
        {
            if (!string.IsNullOrWhiteSpace(dateFrom))
                ads = ads.Where(t => t.DateOfPublication >= Convert.ToDateTime(dateFrom));

            return ads;
        }

        public static IEnumerable<Ad> ByDateTo(this IEnumerable<Ad> ads, string dateTo)
        {
            if (!string.IsNullOrWhiteSpace(dateTo))
                ads = ads.Where(a => a.DateOfPublication <= Convert.ToDateTime(dateTo));

            return ads;
        }

        public static IEnumerable<Ad> ByOnlyMyAds(this IEnumerable<Ad> ads, AdFilterModel model)
        {
            if (model.OnlyMyAds == true)
                ads = ads.Where(a => a.UserId == model.CurrentUser.Id);

            return ads;
        }

        public static IEnumerable<Ad> ByOnlyWithPhotos(this IEnumerable<Ad> ads, bool onlyWithPhotos)
        {
            if (onlyWithPhotos == true)
                ads = ads.Where(a => a.Pictures.ToList().Count >= 1);

            return ads;
        }

        public static IEnumerable<Ad> ByOnlyWithoutPhotos(this IEnumerable<Ad> ads, bool onlyWithoutPhotos)
        {
            if (onlyWithoutPhotos == true)
                ads = ads.Where(a => a.Pictures.ToList().Count == 0);

            return ads;
        }
    }
}
