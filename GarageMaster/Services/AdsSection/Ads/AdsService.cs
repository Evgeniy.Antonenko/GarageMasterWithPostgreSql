﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Pictures;
using GarageMaster.Models.AdsSection.AdsModels;
using GarageMaster.Services.AdsSection.Ads.Contracts;
using GarageMaster.Validation;
using GarageMaster.Validation.Validation_Entities.AdsSection.Ads;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace GarageMaster.Services.AdsSection.Ads
{
    public class AdsService : IAdsService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly AdCreateValidator _adCreateValidator;
        private readonly AdEditValidator _adEditValidator;
        private readonly AdEditAdminValidator _adEditAdminValidator;

        public AdsService(IUnitOfWorkFactory unitOfWorkFactory, AdCreateValidator adCreateValidator, AdEditValidator adEditValidator, AdEditAdminValidator adEditAdminValidator)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _adCreateValidator = adCreateValidator;
            _adEditValidator = adEditValidator;
            _adEditAdminValidator = adEditAdminValidator;
        }

        public AdFilterModel GetAdFilterModel(AdFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Ad> ads = unitOfWork.Ads.GetAllAdsWithAuthorsAndPicturesAndAdCategory().ToList().OrderByDescending(a => a.PickupDate);
                
                ads = ads
                    .ByAdCategoryId(unitOfWork, model.AdCategoryId)
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author)
                    .ByDateFrom(model.DateFrom)
                    .ByDateTo(model.DateTo)
                    .ByOnlyMyAds(model)
                    .ByOnlyWithPhotos(model.OnlyWithPhotos)
                    .ByOnlyWithoutPhotos(model.OnlyWithoutPhotos);

                List<AdModel> adModels = Mapper.Map<List<AdModel>>(ads);
                

                int pageSize = 3;
                int count = ads.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                AdPageModel placePageModel = new AdPageModel(count, page, pageSize);
                adModels = adModels.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                model.IsAuthorized = model.CurrentUser != null;

                model.AdPageModel = placePageModel;
                model.AdCategoriesSelect = GetAdCategoriesSelect();

                model.Ads = adModels;

                return model;
            }
        }

        public AdFilterAdminModel GetAdFilterAdminModel(AdFilterAdminModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Ad> ads = unitOfWork.Ads.GetAllAdsWithAuthorsAndPicturesAndAdCategory().ToList().OrderByDescending(a => a.PickupDate);

                ads = ads
                    .ByAdCategoryId(unitOfWork, model.AdCategoryId)
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author)
                    .ByDateFrom(model.DateFrom)
                    .ByDateTo(model.DateTo);

                List<AdAdminModel> adModels = Mapper.Map<List<AdAdminModel>>(ads);


                int pageSize = 3;
                int count = ads.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                AdPageModel placePageModel = new AdPageModel(count, page, pageSize);
                adModels = adModels.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                model.AdPageModel = placePageModel;
                model.AdCategoriesSelect = GetAdCategoriesSelect();

                model.Ads = adModels;

                return model;
            }
        }

        public AdCreateModel GetAdCreateModel()
        {
            return new AdCreateModel()
            {
                AdCategoriesSelect = GetAdCategoriesSelect()
            };
        }

        public ValidationResult CreateAd(AdCreateModel model, User currentUser)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            var validationResult = _adCreateValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var ad = Mapper.Map<Ad>(model);
                    ad.UserId = currentUser.Id;
                    ad.PickupDate = ad.DateOfPublication;
                    unitOfWork.Ads.Create(ad);

                    var adId = unitOfWork.Ads.GetAdIdByAdTitle(model.Title);
                    UploadPictures(model.Images, currentUser.Id, adId);
                }
            }
            return validationResult;
        }

        public AdEditModel GetAdEditModel(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetById(id);
                var adEditModel = Mapper.Map<AdEditModel>(ad);
                adEditModel.AdCategoriesSelect = GetAdCategoriesSelect();
                return adEditModel;
            }
        }

        public ValidationResult EditAd(AdEditModel model, Guid currentUserId)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            var validationResult = _adEditValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var ad = Mapper.Map<Ad>(model);
                    ad.UserId = currentUserId;

                    unitOfWork.Ads.Update(ad);
                    if (model.RemoveAllPhoto)
                    {
                        DeleteAdPictures(model.Id);
                    }

                    if (model.Images != null)
                    {
                        UploadPictures(model.Images, currentUserId, model.Id);
                    }
                }
            }
            return validationResult;
        }

        public AdEditAdminModel GetAdEditAdminModel(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetById(id);
                var adEditAdminModel = Mapper.Map<AdEditAdminModel>(ad);
                adEditAdminModel.AdCategoriesSelect = GetAdCategoriesSelect();
                return adEditAdminModel;
            }
        }

        public ValidationResult EditAdAdmin(AdEditAdminModel model)
        {
            model.Title = Regex.Replace(model.Title, @"\s+", " ").Trim();
            model.Content = Regex.Replace(model.Content, @"\s+", " ").Trim();
            var validationResult = _adEditAdminValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var ad = Mapper.Map<Ad>(model);
                    ad.UserId = model.UserId;
                    if (model.DiscardPickUpDate)
                    {
                        ad.PickupDate = model.DateOfPublication;
                    }

                    unitOfWork.Ads.Update(ad);
                }
            }
            return validationResult;
        }

        public AdModel GetAdById(Guid id, User currentUser)
        {
            var currentUserId = currentUser == null ? Guid.Empty : currentUser.Id;

            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetAdWithAuthorsAndPicturesAndAdCategoryById(id);
                var adModel = Mapper.Map<AdModel>(ad);
                if (adModel.UserId == currentUserId)
                {
                    adModel.IsAuthor = true;
                }
                else
                {
                    adModel.IsAuthor = false;
                }
                return adModel;
            }
        }

        public void MakePickUpDate(Guid adId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetAdWithAuthorsAndPicturesAndAdCategoryById(adId);
                ad.PickupDate = DateTime.Now;
                unitOfWork.Ads.Update(ad);
            }
        }

        public AdDeleteModel GetAdDeleteModel(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ad = unitOfWork.Ads.GetAdWithAuthorsAndPicturesAndAdCategoryById(id);
                var model = Mapper.Map<AdDeleteModel>(ad);
                model.QtyPhotos = ad.Pictures.ToList().Count;

                return model;
            }
        }

        public void DeleteAd(AdDeleteModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                if (model.QtyPhotos != 0)
                {
                    DeleteAdPictures(model.Id);
                }

                var ad = unitOfWork.Ads.GetById(model.Id);
                unitOfWork.Ads.Remove(ad);
            }
        }

        private void DeleteAdPictures(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var adPictures = unitOfWork.Pictures.GetAdPictureByAdId(id);
                foreach (var adPicture in adPictures)
                {
                    string path = Directory.GetCurrentDirectory() + "/wwwroot" + adPicture.FilePath;
                    File.Delete(path);
                    unitOfWork.Pictures.Remove(adPicture);
                }
            }
        }

        private void UploadPictures(IFormFileCollection images, Guid userId, Guid adId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                if (images != null)
                {
                    foreach (var image in images)
                    {
                        var path = UploadImage(image);
                        var galleryImage = new Picture()
                        {
                            UserId = userId,
                            AdId = adId,
                            FilePath = path,
                            PictureType = PictureType.ForAd
                        };
                        unitOfWork.Pictures.Create(galleryImage);
                    }
                }
            }
        }

        private string UploadImage(IFormFile image)
        {
            string fileName = $"{Guid.NewGuid()}_{image.FileName}";
            string path = @"wwwroot";
            string subpath = @"uploads\AdsSection";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string filePath = dirInfo.CreateSubdirectory(subpath).ToString();
            filePath = $@"wwwroot/uploads/AdsSection/{fileName}";
            using (var stream = System.IO.File.Create(filePath))
            {
                image.CopyTo(stream);
            }
            return $@"/uploads/AdsSection/{fileName}";
        }

        private SelectList GetAdCategoriesSelect()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var adCatagories = unitOfWork.AdCategories.GetAll().ToList();
                return new SelectList(adCatagories, nameof(AdCategory.Id), nameof(AdCategory.AdCategoryName));
            }
        }
    }
}
