﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Entities.Pictures;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Services.Forum.Topics.Contracts;
using GarageMaster.Validation;
using GarageMaster.Validation.Validation_Entities.Forum;
using Microsoft.AspNetCore.Http;

namespace GarageMaster.Services.Forum.Topics
{
    public class TopicService : ITopicService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly TopicCreateValidator _topicCreateValidator;
        private readonly TopicEditValidator _topicEditValidator;

        public TopicService(IUnitOfWorkFactory unitOfWorkFactory, TopicCreateValidator topicCreateValidator, TopicEditValidator topicEditValidator)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (topicCreateValidator == null)
                throw new ArgumentNullException(nameof(topicCreateValidator));
            if (topicEditValidator == null)
                throw new ArgumentNullException(nameof(topicEditValidator));

            _unitOfWorkFactory = unitOfWorkFactory;
            _topicCreateValidator = topicCreateValidator;
            _topicEditValidator = topicEditValidator;
        }

        public TopicFilterModel GetTopicFilterModel(TopicFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                //IEnumerable<Comment> comments = unitOfWork.Comments.GetAllCommentsWithAuthorsAndTopics().ToList();

                IEnumerable<Comment> commentsQty = unitOfWork.Comments.GetAll().ToList();

                //comments = comments.Where(c => c.Type == CommentType.ToTopic);

                IEnumerable<Topic> topics = unitOfWork.Topics.GetAllTopicsWithAuthorsWithCommentsWithPictures().OrderByDescending(t => t.TopicCreationDate).ToList();
                //foreach (var topic in topics)
                //{
                //    topic.Comments = comments.Where(c => c.TopicId == topic.Id).ToList();
                //}

                topics = topics
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author)
                    .ByDateFrom(model.DateFrom)
                    .ByDateTo(model.DateTo)
                    .ByOnlyMyTopics(model);

                List<TopicModel> topicModels = Mapper.Map<List<TopicModel>>(topics);
                //foreach (var topicModel in topicModels)
                //{
                //    topicModel.CommentsQty = commentsQty.Count(c => c.TopicId == topicModel.Id);
                //}

                int pageSize = 5;
                int count = topics.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                TopicPageModel topicPageModel = new TopicPageModel(count, page, pageSize);
                topicModels = topicModels.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                //List<CommentModel> commentModels = Mapper.Map<List<CommentModel>>(comments);

                model.TopicPageModel = topicPageModel;
                //model.Comments = commentModels;
                model.Topics = topicModels;

                model.IsAuthorized = model.CurrentUser != null;

                return model;
            }
        }

        public ValidationResult CreateTopic(TopicCreateModel model, Guid currentUserId)
        {
            model.TopicTitle = Regex.Replace(model.TopicTitle, @"\s+", " ").Trim();
            model.TopicContent = Regex.Replace(model.TopicContent, @"\s+", " ").Trim();
            var validationResult = _topicCreateValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var topic = Mapper.Map<Topic>(model);
                    topic.AuthorId = currentUserId;
                    unitOfWork.Topics.Create(topic);

                    var topicId = unitOfWork.Topics.GetTopicIdByTopicTitle(model.TopicTitle);
                    UploadPictures(model.Images, currentUserId, topicId);
                }
            }
            return validationResult;
        }

        public TopicDetailsModel GetTopicById(Guid id, User currentUser)
        {
            var currentUserId = currentUser == null ? Guid.Empty : currentUser.Id;

            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topic = unitOfWork.Topics.GetTopicWithAuthorWithComment(id);
                var topicModel = Mapper.Map<TopicDetailsModel>(topic);
                topicModel.IsAuthorized = currentUser != null;
                if (topicModel.AuthorId == currentUserId)
                {
                    topicModel.IsAuthor = true;
                }
                else
                {
                    topicModel.IsAuthor = false;
                }
                return topicModel;
            }
        }

        public TopicEditModel GetTopicEditModelById(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topic = unitOfWork.Topics.GetById(id);
                var model = Mapper.Map<TopicEditModel>(topic);

                return model;
            }
        }

        public ValidationResult EditTopic(TopicEditModel model, Guid currentUserId)
        {
            model.TopicTitle = Regex.Replace(model.TopicTitle, @"\s+", " ").Trim();
            model.TopicContent = Regex.Replace(model.TopicContent, @"\s+", " ").Trim();
            var validationResult = _topicEditValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var topic = Mapper.Map<Topic>(model);
                    topic.AuthorId = currentUserId;

                    unitOfWork.Topics.Update(topic);
                    if (model.RemoveAllPhoto)
                    {
                        DeleteTopicPictures(model.Id);
                    }

                    if (model.Images != null)
                    {
                        UploadPictures(model.Images, currentUserId, model.Id);
                    }
                }
            }
            return validationResult;
        }

        private void DeleteTopicPictures(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topicPictures = unitOfWork.Pictures.GetAdPictureByTopicId(id);
                foreach (var topicPicture in topicPictures)
                {
                    string path = Directory.GetCurrentDirectory() + "/wwwroot" + topicPicture.FilePath;
                    File.Delete(path);
                    unitOfWork.Pictures.Remove(topicPicture);
                }
            }
        }

        private void UploadPictures(IFormFileCollection images, Guid userId, Guid topicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                if (images != null)
                {
                    foreach (var image in images)
                    {
                        var path = UploadImage(image);
                        var galleryImage = new Picture()
                        {
                            UserId = userId,
                            TopicId = topicId,
                            FilePath = path,
                            PictureType = PictureType.ForTopic
                        };
                        unitOfWork.Pictures.Create(galleryImage);
                    }
                }
            }
        }

        private string UploadImage(IFormFile image)
        {
            string fileName = $"{Guid.NewGuid()}_{image.FileName}";
            string path = @"wwwroot";
            string subpath = @"uploads\ForumSection";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string filePath = dirInfo.CreateSubdirectory(subpath).ToString();
            filePath = $@"wwwroot/uploads/ForumSection/{fileName}";
            using (var stream = System.IO.File.Create(filePath))
            {
                image.CopyTo(stream);
            }
            return $@"/uploads/ForumSection/{fileName}";
        }
    }
}
