﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.Models.Forum.TopicModels;

namespace GarageMaster.Services.Forum.Topics
{
    public static class TopicServiceExtensions
    {
        public static IEnumerable<Topic> BySearchKey(this IEnumerable<Topic> topics, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                topics = topics.Where(t => t.TopicTitle.Contains(searchKey) || t.TopicContent.Contains(searchKey));

            return topics;
        }

        public static IEnumerable<Topic> ByAuthorName(this IEnumerable<Topic> topics, string authorName)
        {
            if (!string.IsNullOrWhiteSpace(authorName))
                topics = topics.Where(t => t.Author.NickName.Contains(authorName));

            return topics;
        }

        public static IEnumerable<Topic> ByDateFrom(this IEnumerable<Topic> topics, string dateFrom)
        {
            if (!string.IsNullOrWhiteSpace(dateFrom))
                topics = topics.Where(t => t.TopicCreationDate >= Convert.ToDateTime(dateFrom));

            return topics;
        }

        public static IEnumerable<Topic> ByDateTo(this IEnumerable<Topic> topics, string dateTo)
        {
            if (!string.IsNullOrWhiteSpace(dateTo))
                topics = topics.Where(t => t.TopicCreationDate <= Convert.ToDateTime(dateTo));

            return topics;
        }

        public static IEnumerable<Topic> ByOnlyMyTopics(this IEnumerable<Topic> ads, TopicFilterModel model)
        {
            if (model.OnlyMyTopics == true)
                ads = ads.Where(a => a.AuthorId == model.CurrentUser.Id);

            return ads;
        }
    }
}
