﻿using System;
using GarageMaster.DAL.Entities;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Validation;

namespace GarageMaster.Services.Forum.Topics.Contracts
{
    public interface ITopicService
    {
        ValidationResult EditTopic(TopicEditModel model, Guid currentUserId);
        TopicEditModel GetTopicEditModelById(Guid id);
        TopicDetailsModel GetTopicById(Guid id, User currentUser);
        ValidationResult CreateTopic(TopicCreateModel model, Guid currentUserId);
        TopicFilterModel GetTopicFilterModel(TopicFilterModel model);
    }
}
