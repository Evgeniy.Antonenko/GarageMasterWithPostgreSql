﻿using System;
using GarageMaster.Models.Forum.CommentModels;
using GarageMaster.Validation;

namespace GarageMaster.Services.Forum.Comments.Contracts
{
    public interface ICommentService
    {
        ValidationResult CreateComment(CommentCreateModel model, Guid currentUserId);
        CommentCreateModel GetCommentToCommentCreateModel(Guid commentId);
        CommentCreateModel GetCommentToTopicCreateModel(Guid topicId);
    }
}
