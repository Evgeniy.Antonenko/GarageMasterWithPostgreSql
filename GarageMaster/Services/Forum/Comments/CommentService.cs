﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using AutoMapper;
using GarageMaster.DAL;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Entities.Pictures;
using GarageMaster.Models.Forum.CommentModels;
using GarageMaster.Models.Forum.TopicModels;
using GarageMaster.Services.Forum.Comments.Contracts;
using GarageMaster.Validation;
using GarageMaster.Validation.Validation_Entities.Forum;
using Microsoft.AspNetCore.Http;

namespace GarageMaster.Services.Forum.Comments
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly CommentCreateValidator _commentCreateValidator;
        private readonly CommentEditValidator _commentEditValidator;

        public CommentService(IUnitOfWorkFactory unitOfWorkFactory, CommentCreateValidator commentCreateValidator, CommentEditValidator commentEditValidator)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (commentCreateValidator == null)
                throw new ArgumentNullException(nameof(commentCreateValidator));
            if (commentEditValidator == null)
                throw new ArgumentNullException(nameof(commentEditValidator));

            _unitOfWorkFactory = unitOfWorkFactory;
            _commentCreateValidator = commentCreateValidator;
            _commentEditValidator = commentEditValidator;
        }

        public CommentCreateModel GetCommentToTopicCreateModel(Guid topicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topic = unitOfWork.Topics.GetTopicWithAuthorWithComment(topicId);

                var commentCreateModel = new CommentCreateModel()
                {
                    TopicId = topicId,
                    CommentLevel = 1,
                    Type = CommentType.ToTopic,

                    TopicModel = Mapper.Map<TopicModel>(topic)
                };

                return commentCreateModel;
            }
        }

        public CommentCreateModel GetCommentToCommentCreateModel(Guid commentId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var parentComment = unitOfWork.Comments.GetFullCommentByCommentId(commentId);
                var topic = unitOfWork.Topics.GetTopicWithAuthorWithComment(parentComment.TopicId);
                var commentCreateModel = new CommentCreateModel()
                {
                    TopicId = parentComment.TopicId,
                    UpperCommentId = parentComment.Id,
                    CommentLevel = parentComment.CommentLevel + 1,
                    Type = CommentType.ToComment,

                    TopicModel = Mapper.Map<TopicModel>(topic)
                };

                return commentCreateModel;
            }
        }

        public ValidationResult CreateComment(CommentCreateModel model, Guid currentUserId)
        {
            model.CommentContent = Regex.Replace(model.CommentContent, @"\s+", " ").Trim();
            var validationResult = _commentCreateValidator.Validate(model);
            if (validationResult.IsSuccess)
            {
                using (var unitOfWork = _unitOfWorkFactory.Create())
                {
                    var comment = Mapper.Map<Comment>(model);
                    comment.AuthorId = currentUserId;
                    unitOfWork.Comments.Create(comment);

                    var commentId = unitOfWork.Comments.GetCommentIdByCommentContent(model.CommentContent);
                    UploadPictures(model.Images, currentUserId, commentId);
                }
            }
            return validationResult;
        }


        private void DeleteCommentPictures(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var commentPictures = unitOfWork.Pictures.GetAdPictureByCommentId(id);
                foreach (var commentPicture in commentPictures)
                {
                    string path = Directory.GetCurrentDirectory() + "/wwwroot" + commentPicture.FilePath;
                    File.Delete(path);
                    unitOfWork.Pictures.Remove(commentPicture);
                }
            }
        }

        private void UploadPictures(IFormFileCollection images, Guid userId, Guid commentId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                if (images != null)
                {
                    foreach (var image in images)
                    {
                        var path = UploadImage(image);
                        var galleryImage = new Picture()
                        {
                            UserId = userId,
                            CommentId = commentId,
                            FilePath = path,
                            PictureType = PictureType.ForComment
                        };
                        unitOfWork.Pictures.Create(galleryImage);
                    }
                }
            }
        }

        private string UploadImage(IFormFile image)
        {
            string fileName = $"{Guid.NewGuid()}_{image.FileName}";
            string path = @"wwwroot";
            string subpath = @"uploads\ForumSection";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
            string filePath = dirInfo.CreateSubdirectory(subpath).ToString();
            filePath = $@"wwwroot/uploads/ForumSection/{fileName}";
            using (var stream = System.IO.File.Create(filePath))
            {
                image.CopyTo(stream);
            }
            return $@"/uploads/ForumSection/{fileName}";
        }
    }
}
