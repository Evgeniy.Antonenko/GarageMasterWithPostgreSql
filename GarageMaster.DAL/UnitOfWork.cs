﻿using System;
using GarageMaster.DAL.Repositories;
using GarageMaster.DAL.Repositories.AboutSiteSection;
using GarageMaster.DAL.Repositories.AdsSection;
using GarageMaster.DAL.Repositories.Contracts;
using GarageMaster.DAL.Repositories.Contracts.AboutSiteSection;
using GarageMaster.DAL.Repositories.Contracts.AdsSection;
using GarageMaster.DAL.Repositories.Contracts.Forum;
using GarageMaster.DAL.Repositories.Contracts.Pictures;
using GarageMaster.DAL.Repositories.Forum;
using GarageMaster.DAL.Repositories.PIctures;

namespace GarageMaster.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IUserRepository User { get; set; }
        public IRoleRepository Role { get; set; }
        public IContactRepository Contacts { get; set; }
        public IAboutProjectRepository AboutProjects { get; set; }
        public IRuleRepository Rules { get; set; }
        public IAdRepository Ads { get; set; }
        public IAdCategoryRepository AdCategories { get; set; }
        public ITopicRepository Topics { get; set; }
        public ICommentRepository Comments { get; set; }
        public IPictureRepository Pictures { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            User = new UserRepository(context);
            Role = new RoleRepository(context);
            Contacts = new ContactRepository(context);
            AboutProjects = new AboutProjectRepository(context);
            Rules = new RuleRepository(context);
            Ads = new AdRepository(context);
            AdCategories = new AdCategoryRepository(context);
            Topics = new TopicRepository(context);
            Comments = new CommentRepository(context);
            Pictures = new PictureRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
