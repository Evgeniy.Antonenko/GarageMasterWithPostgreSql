﻿using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.DAL.Repositories.Contracts.AboutSiteSection;

namespace GarageMaster.DAL.Repositories.AboutSiteSection
{
    public class RuleRepository : Repository<Rule>, IRuleRepository
    {
        public RuleRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Rules;
        }
    }
}
