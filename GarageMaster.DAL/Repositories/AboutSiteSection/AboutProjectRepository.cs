﻿using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.DAL.Repositories.Contracts.AboutSiteSection;

namespace GarageMaster.DAL.Repositories.AboutSiteSection
{
    public class AboutProjectRepository : Repository<AboutProject>, IAboutProjectRepository
    {
        public AboutProjectRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.AboutProjects;
        }
    }
}
