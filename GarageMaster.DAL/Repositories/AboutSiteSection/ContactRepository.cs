﻿using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.DAL.Repositories.Contracts.AboutSiteSection;

namespace GarageMaster.DAL.Repositories.AboutSiteSection
{
    public class ContactRepository : Repository<Contact>, IContactRepository
    {
        public ContactRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Contacts;
        }
    }
}
