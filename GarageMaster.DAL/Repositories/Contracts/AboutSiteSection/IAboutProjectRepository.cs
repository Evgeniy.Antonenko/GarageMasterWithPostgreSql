﻿using GarageMaster.DAL.Entities.AboutSiteSection;

namespace GarageMaster.DAL.Repositories.Contracts.AboutSiteSection
{
    public interface IAboutProjectRepository : IRepository<AboutProject>
    {
    }
}
