﻿using GarageMaster.DAL.Entities.AboutSiteSection;

namespace GarageMaster.DAL.Repositories.Contracts.AboutSiteSection
{
    public interface IContactRepository : IRepository<Contact>
    {
    }
}
