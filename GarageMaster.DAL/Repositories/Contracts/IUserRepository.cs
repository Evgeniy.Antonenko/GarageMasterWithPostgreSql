﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities;

namespace GarageMaster.DAL.Repositories.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetUsersWithoutById(Guid id);
        User GetByUserName(string email);
        User GetByUserNickName(string nickName);
        User GetByMobilePhone(string mobilePhone);
    }
}
