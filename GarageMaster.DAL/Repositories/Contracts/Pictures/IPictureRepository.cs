﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.Pictures;

namespace GarageMaster.DAL.Repositories.Contracts.Pictures
{
    public interface IPictureRepository : IRepository<Picture>
    {
        IEnumerable<Picture> GetAdPictureByAdId(Guid id);
        IEnumerable<Picture> GetAdPictureByTopicId(Guid id);
        IEnumerable<Picture> GetAdPictureByCommentId(Guid id);
    }
}
