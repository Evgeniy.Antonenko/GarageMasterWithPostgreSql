﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities.Contracts;

namespace GarageMaster.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T : class, IEntity
    {
        T Create(T entity);

        T GetById(Guid id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);

        Task<T> GetByIdAsync(Guid id);

        Task CreateAsync(T entity);
    }
}
