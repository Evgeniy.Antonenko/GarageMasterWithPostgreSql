﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.Forum;

namespace GarageMaster.DAL.Repositories.Contracts.Forum
{
    public interface ICommentRepository : IRepository<Comment>
    {
        Guid GetCommentIdByCommentContent(string commentContent);
        Comment GetByCommentContent(string commentContent);
        Comment GetFullCommentByCommentId(Guid commentId);
        IEnumerable<Comment> GetAllCommentsWithAuthorsAndTopics();
        IEnumerable<Comment> GetCommentsWithAuthorsAndTopicsByTopicId(Guid topicId);
    }
}
