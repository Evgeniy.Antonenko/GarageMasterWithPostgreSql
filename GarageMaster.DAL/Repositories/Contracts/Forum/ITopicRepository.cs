﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.Forum;

namespace GarageMaster.DAL.Repositories.Contracts.Forum
{
    public interface ITopicRepository : IRepository<Topic>
    {
        Guid GetTopicIdByTopicTitle(string title);
        Topic GetByTopicTitle(string topicTitle);
        IEnumerable<Topic> GetTopicsWithoutById(Guid id);
        Topic GetTopicWithAuthorWithComment(Guid id);
        IEnumerable<Topic> GetAllTopicsWithAuthorsWithCommentsWithPictures();
    }
}
