﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.AdsSection;

namespace GarageMaster.DAL.Repositories.Contracts.AdsSection
{
    public interface IAdRepository : IRepository<Ad>
    {
        Ad GetAdByTitle(string title);
        Ad GetAdByContent(string content);
        IEnumerable<Ad> GetAdWithoutById(Guid id);
        Guid GetAdIdByAdTitle(string title);
        Ad GetAdWithAuthorsAndPicturesAndAdCategoryById(Guid id);
        IEnumerable<Ad> GetAllAdsWithAuthorsAndPicturesAndAdCategory();
    }
}
