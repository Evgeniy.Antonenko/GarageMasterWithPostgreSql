﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.AdsSection;

namespace GarageMaster.DAL.Repositories.Contracts.AdsSection
{
    public interface IAdCategoryRepository : IRepository<AdCategory>
    {
        AdCategory GetByAdCategoryName(string adCategoryName);
        IEnumerable<AdCategory> GetAdCategoryWithoutById(Guid id);
    }
}
