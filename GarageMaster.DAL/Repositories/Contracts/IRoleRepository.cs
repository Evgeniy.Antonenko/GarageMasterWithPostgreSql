﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities;

namespace GarageMaster.DAL.Repositories.Contracts
{
    public interface IRoleRepository : IRepository<Role>
    {
        IEnumerable<Role> GetRolesWithoutById(Guid id);
    }
}
