﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Repositories.Contracts;

namespace GarageMaster.DAL.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Roles;
        }

        public IEnumerable<Role> GetRolesWithoutById(Guid id)
        {
            return entities.Where(r => r.Id != id).ToList();
        }
    }
}
