﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Repositories.Contracts.AdsSection;

namespace GarageMaster.DAL.Repositories.AdsSection
{
    public class AdCategoryRepository : Repository<AdCategory>, IAdCategoryRepository
    {
        public AdCategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.AdCategories;
        }

        public AdCategory GetByAdCategoryName(string adCategoryName)
        {
            return entities.FirstOrDefault(p => p.AdCategoryName == adCategoryName);
        }

        public IEnumerable<AdCategory> GetAdCategoryWithoutById(Guid id)
        {
            return entities.Where(rp => rp.Id != id).ToList();
        }
    }
}
