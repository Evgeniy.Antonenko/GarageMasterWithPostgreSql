﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Repositories.Contracts.AdsSection;
using Microsoft.EntityFrameworkCore;

namespace GarageMaster.DAL.Repositories.AdsSection
{
    public class AdRepository : Repository<Ad>, IAdRepository
    {
        public AdRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Ads;
        }

        public Ad GetAdByTitle(string title)
        {
            return entities.FirstOrDefault(a => a.Title == title);
        }

        public Ad GetAdByContent(string content)
        {
            return entities.FirstOrDefault(a => a.Content == content);
        }

        public IEnumerable<Ad> GetAdWithoutById(Guid id)
        {
            return entities.Where(a => a.Id != id).ToList();
        }

        public Guid GetAdIdByAdTitle(string title)
        {
            var ad = entities.FirstOrDefault(a => a.Title == title);

            return ad.Id;
        }

        public Ad GetAdWithAuthorsAndPicturesAndAdCategoryById(Guid id)
        {
            return GetAllAdsWithAuthorsAndPicturesAndAdCategory().FirstOrDefault(a => a.Id == id);
        }

        public IEnumerable<Ad> GetAllAdsWithAuthorsAndPicturesAndAdCategory()
        {
            return entities
                .Include(a => a.Pictures)
                .Include(a => a.User)
                .Include(a => a.AdCategory)
                .ToList();
        }
    }
}
