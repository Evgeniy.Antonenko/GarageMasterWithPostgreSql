﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Repositories.Contracts.Forum;
using Microsoft.EntityFrameworkCore;

namespace GarageMaster.DAL.Repositories.Forum
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Comments;
        }

        public IEnumerable<Comment> GetAllCommentsWithAuthorsAndTopics()
        {
            return entities.Include(p => p.Author)
                .Include(p => p.Topic)
                .Include(p => p.SubComments);
        }

        public IEnumerable<Comment> GetCommentsWithAuthorsAndTopicsByTopicId(Guid topicId)
        {
            return GetAllCommentsWithAuthorsAndTopics().Where(p => p.TopicId == topicId);
        }

        public Comment GetFullCommentByCommentId(Guid commentId)
        {
            return GetAllCommentsWithAuthorsAndTopics().FirstOrDefault(p => p.Id == commentId);
        }

        public Comment GetByCommentContent(string commentContent)
        {
            return entities.FirstOrDefault(c => c.CommentContent == commentContent);
        }

        public Guid GetCommentIdByCommentContent(string commentContent)
        {
            return GetByCommentContent(commentContent).Id;
        }
    }
}
