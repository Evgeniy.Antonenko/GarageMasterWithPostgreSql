﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Repositories.Contracts.Forum;
using Microsoft.EntityFrameworkCore;

namespace GarageMaster.DAL.Repositories.Forum
{
    public class TopicRepository : Repository<Topic>, ITopicRepository
    {
        public TopicRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Topics;
        }

        public IEnumerable<Topic> GetAllTopicsWithAuthorsWithCommentsWithPictures()
        {
            return entities.Include(t => t.Author)
                .Include(t => t.Pictures)
                .Include(t => t.Comments)
                .ThenInclude(t => t.SubComments).ToList();
        }

        public Topic GetTopicWithAuthorWithComment(Guid id)
        {
            return GetAllTopicsWithAuthorsWithCommentsWithPictures().FirstOrDefault(t => t.Id == id);
        }

        public Topic GetByTopicTitle(string topicTitle)
        {
            return entities.FirstOrDefault(t => t.TopicTitle == topicTitle);
        }

        public IEnumerable<Topic> GetTopicsWithoutById(Guid id)
        {
            return entities.Where(t => t.Id != id).ToList();
        }

        public Guid GetTopicIdByTopicTitle(string topicTitle)
        {
            return GetByTopicTitle(topicTitle).Id;
        }
    }
}
