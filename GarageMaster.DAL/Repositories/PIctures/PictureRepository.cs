﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities.Pictures;
using GarageMaster.DAL.Repositories.Contracts.Pictures;

namespace GarageMaster.DAL.Repositories.PIctures
{
    public class PictureRepository : Repository<Picture>, IPictureRepository
    {
        public PictureRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Pictures;
        }

        public IEnumerable<Picture> GetAdPictureByAdId(Guid id)
        {
            return entities.Where(ap => ap.AdId == id).ToList();
        }

        public IEnumerable<Picture> GetAdPictureByTopicId(Guid id)
        {
            return entities.Where(p => p.TopicId == id).ToList();
        }

        public IEnumerable<Picture> GetAdPictureByCommentId(Guid id)
        {
            return entities.Where(p => p.CommentId == id).ToList();
        }
    }
}
