﻿using System;
using System.Collections.Generic;
using System.Linq;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Repositories.Contracts;

namespace GarageMaster.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Users;
        }

        public IEnumerable<User> GetUsersWithoutById(Guid id)
        {
            return entities.Where(rp => rp.Id != id).ToList();
        }

        public User GetByUserName(string email)
        {
            return entities.FirstOrDefault(u => u.UserName == email);
        }

        public User GetByUserNickName(string nickName)
        {
            return entities.FirstOrDefault(u => u.NickName == nickName);
        }

        public User GetByMobilePhone(string mobilePhone)
        {
            return entities.FirstOrDefault(u => u.MobilePhone == mobilePhone);
        }
    }
}
