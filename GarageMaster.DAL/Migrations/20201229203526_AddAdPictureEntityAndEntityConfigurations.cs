﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GarageMaster.DAL.Migrations
{
    public partial class AddAdPictureEntityAndEntityConfigurations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AdPictures",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    AdId = table.Column<Guid>(nullable: false),
                    FilePath = table.Column<string>(maxLength: 2147483647, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdPictures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdPictures_Ads_AdId",
                        column: x => x.AdId,
                        principalTable: "Ads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AdPictures_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdPictures_AdId",
                table: "AdPictures",
                column: "AdId");

            migrationBuilder.CreateIndex(
                name: "IX_AdPictures_Id",
                table: "AdPictures",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdPictures_UserId",
                table: "AdPictures",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdPictures");
        }
    }
}
