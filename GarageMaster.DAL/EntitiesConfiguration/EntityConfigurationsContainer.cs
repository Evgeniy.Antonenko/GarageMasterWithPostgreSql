﻿using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Entities.Pictures;
using GarageMaster.DAL.EntitiesConfiguration.AboutSiteSection;
using GarageMaster.DAL.EntitiesConfiguration.AdsSection;
using GarageMaster.DAL.EntitiesConfiguration.Contracts;
using GarageMaster.DAL.EntitiesConfiguration.Forum;
using GarageMaster.DAL.EntitiesConfiguration.Pictures;

namespace GarageMaster.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<User> UserConfiguration { get; set; }
        public IEntityConfiguration<Role> RoleConfiguration { get; set; }
        public IEntityConfiguration<Contact> ContactConfiguration { get; }
        public IEntityConfiguration<AboutProject> AboutProjectConfiguration { get; }
        public IEntityConfiguration<Rule> RuleConfiguration { get; }
        public IEntityConfiguration<Ad> AdConfiguration { get; }
        public IEntityConfiguration<AdCategory> AdCategoryConfiguration { get; }
        public IEntityConfiguration<Topic> TopicConfiguration { get; }
        public IEntityConfiguration<Comment> CommentConfiguration { get; }
        public IEntityConfiguration<Picture> PictureConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            UserConfiguration = new UserConfiguration();
            RoleConfiguration = new RoleConfiguration();
            ContactConfiguration = new ContactConfiguration();
            AboutProjectConfiguration = new AboutProjectConfiguration();
            RuleConfiguration = new RuleConfiguration();
            AdConfiguration = new AdConfiguration();
            AdCategoryConfiguration = new AdCategoryConfiguration();
            TopicConfiguration = new TopicConfiguration();
            CommentConfiguration = new CommentConfiguration();
            PictureConfiguration = new PictureConfiguration();
        }
    }
}
