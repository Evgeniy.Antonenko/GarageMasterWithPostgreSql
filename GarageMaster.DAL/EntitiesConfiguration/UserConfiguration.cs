﻿using GarageMaster.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration
{
    public class UserConfiguration : BaseEntityConfiguration<User>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<User> builder)
        {
            builder
                .Property(u => u.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(u => u.Surname)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(u => u.NickName)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(u => u.MobilePhone)
                .HasMaxLength(13)
                .IsRequired();

            builder
                .Property(u => u.Photo)
                .HasMaxLength(500)
                .HasDefaultValue(@"/assets/empty_avatar.jpg")
                .IsRequired();

            builder
                .Property(c => c.CreatedOn)
                .IsRequired();

            builder
                .Property(p => p.IsActive)
                .HasDefaultValue(true);

            builder
                .Property(b => b.CreatedOn)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<User> builder)
        {
            builder
                .HasMany(u => u.Ads)
                .WithOne(a => a.User)
                .HasForeignKey(a => a.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(u => u.Topics)
                .WithOne(t => t.Author)
                .HasForeignKey(t => t.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(u => u.Comments)
                .WithOne(c => c.Author)
                .HasForeignKey(c => c.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(u => u.Pictures)
                .WithOne(p => p.User)
                .HasForeignKey(p => p.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
