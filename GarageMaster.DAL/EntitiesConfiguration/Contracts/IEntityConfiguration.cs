﻿using System;
using GarageMaster.DAL.Entities.Contracts;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
