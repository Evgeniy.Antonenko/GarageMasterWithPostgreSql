﻿using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Entities.Pictures;

namespace GarageMaster.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<User> UserConfiguration { get; set; }
        IEntityConfiguration<Role> RoleConfiguration { get; set; }
        IEntityConfiguration<Contact> ContactConfiguration { get; }
        IEntityConfiguration<AboutProject> AboutProjectConfiguration { get; }
        IEntityConfiguration<Rule> RuleConfiguration { get; }
        IEntityConfiguration<Ad> AdConfiguration { get; }
        IEntityConfiguration<AdCategory> AdCategoryConfiguration { get; }
        IEntityConfiguration<Topic> TopicConfiguration { get; }
        IEntityConfiguration<Comment> CommentConfiguration { get; }
        IEntityConfiguration<Picture> PictureConfiguration { get; }
    }
}
