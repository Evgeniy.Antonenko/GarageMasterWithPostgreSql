﻿using GarageMaster.DAL.Entities.Pictures;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration.Pictures
{
    public class PictureConfiguration : BaseEntityConfiguration<Picture>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Picture> builder)
        {
            builder
                .Property(p => p.FilePath)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(p => p.UserId)
                .IsRequired();

            builder
                .Property(p => p.AdId)
                .IsRequired(false);

            builder
                .Property(p => p.TopicId)
                .IsRequired(false);

            builder
                .Property(p => p.CommentId)
                .IsRequired(false);

            builder
                .HasIndex(p => p.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Picture> builder)
        {
            builder
                .HasOne(p => p.User)
                .WithMany(u => u.Pictures)
                .HasForeignKey(ap => ap.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(p => p.Ad)
                .WithMany(a => a.Pictures)
                .HasForeignKey(p => p.AdId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(p => p.Topic)
                .WithMany(t => t.Pictures)
                .HasForeignKey(p => p.TopicId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(p => p.Comment)
                .WithMany(c => c.Pictures)
                .HasForeignKey(p => p.CommentId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
