﻿using GarageMaster.DAL.Entities.AboutSiteSection;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration.AboutSiteSection
{
    public class ContactConfiguration : BaseEntityConfiguration<Contact>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Contact> builder)
        {
            builder
                .Property(con => con.Title)
                .HasMaxLength(250)
                .IsRequired();

            builder
                .Property(con => con.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(con => con.Address)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(con => con.Email)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(con => con.PhoneNumber)
                .HasMaxLength(13)
                .IsRequired();

            builder
                .Property(con => con.FilePath)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .HasIndex(con => con.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Contact> builder)
        {
        }
    }
}
