﻿using GarageMaster.DAL.Entities.AdsSection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration.AdsSection
{
    public class AdCategoryConfiguration : BaseEntityConfiguration<AdCategory>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<AdCategory> builder)
        {
            builder
                .Property(ac => ac.AdCategoryName)
                .HasMaxLength(150)
                .IsRequired();

            builder
                .HasIndex(ac => ac.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<AdCategory> builder)
        {
            builder
                .HasMany(ac => ac.Ads)
                .WithOne(a => a.AdCategory)
                .HasForeignKey(a => a.AdCategoryId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
