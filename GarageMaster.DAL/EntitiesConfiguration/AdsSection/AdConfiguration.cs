﻿using GarageMaster.DAL.Entities.AdsSection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration.AdsSection
{
    public class AdConfiguration : BaseEntityConfiguration<Ad>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Ad> builder)
        {
            builder
                .Property(a => a.Title)
                .HasMaxLength(450)
                .IsRequired();

            builder
                .Property(a => a.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(a => a.DateOfPublication)
                .IsRequired();

            builder
                .Property(a => a.PickupDate)
                .IsRequired();

            builder
                .HasIndex(ac => ac.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Ad> builder)
        {
            builder
                .HasOne(a => a.User)
                .WithMany(ac => ac.Ads)
                .HasForeignKey(a => a.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(a => a.AdCategory)
                .WithMany(ac => ac.Ads)
                .HasForeignKey(a => a.AdCategoryId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasMany(a => a.Pictures)
                .WithOne(p => p.Ad)
                .HasForeignKey(ap => ap.AdId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
