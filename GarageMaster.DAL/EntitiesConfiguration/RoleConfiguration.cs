﻿using GarageMaster.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration
{
    public class RoleConfiguration : BaseEntityConfiguration<Role>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Role> builder)
        {
            builder
                .Property(r => r.Name)
                .HasMaxLength(100)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Role> builder)
        {
        }
    }
}
