﻿using GarageMaster.DAL.Entities.Forum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration.Forum
{
    public class CommentConfiguration : BaseEntityConfiguration<Comment>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Comment> builder)
        {
            builder
                .Property(b => b.CommentContent)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(t => t.CommentCreationDate)
                .IsRequired();

            builder
                .HasIndex(t => t.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasOne(c => c.UpperComment)
                .WithMany(c => c.SubComments)
                .HasForeignKey(c => c.UpperCommentId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false);

            builder
                .HasMany(c => c.SubComments)
                .WithOne(c => c.UpperComment)
                .HasForeignKey(c => c.UpperCommentId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(c => c.Author)
                .WithMany(u => u.Comments)
                .HasForeignKey(c => c.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(c => c.Topic)
                .WithMany(t => t.Comments)
                .HasForeignKey(t => t.TopicId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(c => c.Pictures)
                .WithOne(p => p.Comment)
                .HasForeignKey(c => c.CommentId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
