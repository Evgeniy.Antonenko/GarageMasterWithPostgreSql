﻿using GarageMaster.DAL.Entities.Forum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GarageMaster.DAL.EntitiesConfiguration.Forum
{
    public class TopicConfiguration : BaseEntityConfiguration<Topic>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Topic> builder)
        {
            builder
                .Property(t => t.TopicTitle)
                .HasMaxLength(300)
                .IsRequired();
            builder
                .Property(t => t.TopicContent)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(t => t.TopicCreationDate)
                .IsRequired();
            builder
                .Property(t => t.IsLocked)
                .HasDefaultValue(false);
            builder
                .HasIndex(t => t.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Topic> builder)
        {
            builder
                .HasOne(t => t.Author)
                .WithMany(u => u.Topics)
                .HasForeignKey(t => t.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasMany(t => t.Comments)
                .WithOne(c => c.Topic)
                .HasForeignKey(c => c.TopicId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(t => t.Pictures)
                .WithOne(p => p.Topic)
                .HasForeignKey(p => p.TopicId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
