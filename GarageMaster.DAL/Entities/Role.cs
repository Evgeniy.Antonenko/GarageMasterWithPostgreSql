﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using GarageMaster.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.DAL.Entities
{
    public class Role : IdentityRole<Guid>, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Guid Id { get; set; }
        public override string Name { get; set; }
    }
}
