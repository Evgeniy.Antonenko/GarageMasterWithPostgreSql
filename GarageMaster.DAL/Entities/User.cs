﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Contracts;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Entities.Pictures;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.DAL.Entities
{
    public class User : IdentityUser<Guid>, IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override Guid Id { get; set; }
        public override string UserName { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string MobilePhone { get; set; }
        public string Photo { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Ad> Ads { get; set; }
        public ICollection<Topic> Topics { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Picture> Pictures { get; set; }
    }
}
