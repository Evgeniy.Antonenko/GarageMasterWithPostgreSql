﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.Contracts;
using GarageMaster.DAL.Entities.Pictures;

namespace GarageMaster.DAL.Entities.AdsSection
{
    public class Ad : IEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime DateOfPublication { get; set; }
        public DateTime PickupDate { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public Guid AdCategoryId { get; set; }
        public AdCategory AdCategory { get; set; }
        public ICollection<Picture> Pictures { get; set; }
    }
}
