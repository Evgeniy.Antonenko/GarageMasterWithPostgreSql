﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.Contracts;

namespace GarageMaster.DAL.Entities.AdsSection
{
    public class AdCategory : IEntity
    {
        public Guid Id { get; set; }
        public string AdCategoryName { get; set; }
        public ICollection<Ad> Ads { get; set; }
    }
}
