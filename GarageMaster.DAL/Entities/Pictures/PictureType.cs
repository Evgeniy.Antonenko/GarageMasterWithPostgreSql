﻿namespace GarageMaster.DAL.Entities.Pictures
{
    public enum PictureType
    {
        ForAd,
        ForTopic,
        ForComment
    }
}
