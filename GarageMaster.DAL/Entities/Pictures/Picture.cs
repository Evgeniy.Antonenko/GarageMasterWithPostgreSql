﻿using System;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Contracts;
using GarageMaster.DAL.Entities.Forum;

namespace GarageMaster.DAL.Entities.Pictures
{
    public class Picture : IEntity
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public Guid? AdId { get; set; }
        public Ad Ad { get; set; }
        public Guid? TopicId { get; set; }
        public Topic Topic { get; set; }
        public Guid? CommentId { get; set; }
        public Comment Comment { get; set; }
        public PictureType PictureType { get; set; }
        public string FilePath { get; set; }
    }
}
