﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.Contracts;
using GarageMaster.DAL.Entities.Pictures;

namespace GarageMaster.DAL.Entities.Forum
{
    public class Topic : IEntity
    {
        public Guid Id { get; set; }
        public DateTime TopicCreationDate { get; set; }
        public string TopicTitle { get; set; }
        public string TopicContent { get; set; }
        public bool IsLocked { get; set; }
        public Guid AuthorId { get; set; }
        public User Author { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Picture> Pictures { get; set; }
    }
}
