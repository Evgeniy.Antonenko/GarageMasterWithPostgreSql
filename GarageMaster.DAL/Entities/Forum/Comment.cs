﻿using System;
using System.Collections.Generic;
using GarageMaster.DAL.Entities.Contracts;
using GarageMaster.DAL.Entities.Pictures;

namespace GarageMaster.DAL.Entities.Forum
{
    public class Comment : IEntity
    {
        public Guid Id { get; set; }
        public DateTime CommentCreationDate { get; set; }
        public string CommentContent { get; set; }
        public Guid AuthorId { get; set; }
        public User Author { get; set; }
        public Guid TopicId { get; set; }
        public Topic Topic { get; set; }
        public Guid? UpperCommentId { get; set; }
        public Comment UpperComment { get; set; }
        public int CommentLevel { get; private set; }
        public CommentType Type { get; private set; }
        public ICollection<Comment> SubComments { get; set; }
        public ICollection<Picture> Pictures { get; set; }
    }
}
