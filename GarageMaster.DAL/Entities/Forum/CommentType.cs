﻿namespace GarageMaster.DAL.Entities.Forum
{
    public enum CommentType
    {
        ToTopic,
        ToComment
    }
}
