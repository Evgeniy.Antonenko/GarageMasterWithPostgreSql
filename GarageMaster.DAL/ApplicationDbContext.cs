﻿using System;
using GarageMaster.DAL.Entities;
using GarageMaster.DAL.Entities.AboutSiteSection;
using GarageMaster.DAL.Entities.AdsSection;
using GarageMaster.DAL.Entities.Forum;
using GarageMaster.DAL.Entities.Pictures;
using GarageMaster.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GarageMaster.DAL
{

    public class ApplicationDbContext : IdentityDbContext<User, Role, Guid>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<AboutProject> AboutProjects { get; set; }
        public DbSet<Rule> Rules { get; set; }
        public DbSet<Ad> Ads { get; set; }
        public DbSet<AdCategory> AdCategories { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Picture> Pictures { get; set; }


        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.UserConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.RoleConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.ContactConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.AboutProjectConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.RuleConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.AdConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.AdCategoryConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.TopicConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.CommentConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.PictureConfiguration.ProvideConfigurationAction());
        }
    }
}
