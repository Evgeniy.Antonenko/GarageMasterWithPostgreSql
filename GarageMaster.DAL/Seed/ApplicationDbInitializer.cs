﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GarageMaster.DAL.Entities;
using Microsoft.AspNetCore.Identity;

namespace GarageMaster.DAL.Seed
{
    public static class ApplicationDbInitializer
    {
        public static async Task SeedRoles(RoleManager<Role> roleManager)
        {
            var roles = Enum.GetValues(typeof(Roles)).OfType<Roles>().ToList().Select(value => value.ToString());
            foreach (var role in roles)
            {
                var roleCheck = await roleManager.RoleExistsAsync(role);
                if (!roleCheck)
                {
                    await roleManager.CreateAsync(new Role());
                }
            }
        }
    }
}
